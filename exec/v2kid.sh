#!/bin/bash

function v2ray_f(){
apt install jq -y
BEIJING_UPDATE_TIME=3
BEGIN_PATH=$(pwd)
INSTALL_WAY=0
HELP=0
REMOVE=0
CHINESE=0
BASE_SOURCE_PATH="https://multi.netlify.app"
UTIL_PATH="/etc/v2ray_util/util.cfg"
UTIL_CFG="$BASE_SOURCE_PATH/v2ray_util/util_core/util.cfg"
BASH_COMPLETION_SHELL="$BASE_SOURCE_PATH/v2ray"
CLEAN_IPTABLES_SHELL="$BASE_SOURCE_PATH/v2ray_util/global_setting/clean_iptables.sh"
#Centos 临时取消别名
[[ -f /etc/redhat-release && -z $(echo $SHELL|grep zsh) ]] && unalias -a
[[ -z $(echo $SHELL|grep zsh) ]] && ENV_FILE=".bashrc" || ENV_FILE=".zshrc"
#######color code########
RED="31m"
GREEN="32m"
YELLOW="33m"
BLUE="36m"
FUCHSIA="35m"

colorEcho(){
    COLOR=$1
    echo -e "\033[${COLOR}${@:2}\033[0m"
}

#######get params#########
while [[ $# > 0 ]];do
    key="$1"
    case $key in
        --remove)
        REMOVE=1
        ;;
        -h|--help)
        HELP=1
        ;;
        -k|--keep)
        INSTALL_WAY=1
        colorEcho ${BLUE} "keep config to update\n"
        ;;
        --zh)
        CHINESE=0
        colorEcho ${BLUE} "VERSION CHINA..\n"
        ;;
        *)
                # unknown option
        ;;
    esac
    shift # past argument or value
done
#############################

help(){
    echo "bash v2ray.sh [-h|--help] [-k|--keep] [--remove]"
    echo "  -h, --help           Show help"
    echo "  -k, --keep           keep the config.json to update"
    echo "      --remove         remove v2ray,xray && multi-v2ray"
    echo "                       no params to new install"
    return 0
}

removeV2Ray() {
    #卸载V2ray脚本
    bash <(curl -L -s https://multi.netlify.app/go.sh) --remove >/dev/null 2>&1
    rm -rf /etc/v2ray >/dev/null 2>&1
    rm -rf /var/log/v2ray >/dev/null 2>&1

    #卸载Xray脚本
    bash <(curl -L -s https://multi.netlify.app/go.sh) --remove -x >/dev/null 2>&1
    rm -rf /etc/xray >/dev/null 2>&1
    rm -rf /var/log/xray >/dev/null 2>&1

    #清理v2ray相关iptable规则
    bash <(curl -L -s $CLEAN_IPTABLES_SHELL)

    #卸载multi-v2ray
    pip uninstall v2ray_util -y
    rm -rf /usr/share/bash-completion/completions/v2ray.bash >/dev/null 2>&1
    rm -rf /usr/share/bash-completion/completions/v2ray >/dev/null 2>&1
    rm -rf /usr/share/bash-completion/completions/xray >/dev/null 2>&1
    rm -rf /etc/bash_completion.d/v2ray.bash >/dev/null 2>&1
    rm -rf /usr/local/bin/v2ray >/dev/null 2>&1
    rm -rf /etc/v2ray_util >/dev/null 2>&1
    rm -rf /etc/profile.d/iptables.sh >/dev/null 2>&1
    rm -rf /root/.iptables >/dev/null 2>&1

    #删除v2ray定时更新任务
    crontab -l|sed '/SHELL=/d;/v2ray/d'|sed '/SHELL=/d;/xray/d' > crontab.txt
    crontab crontab.txt >/dev/null 2>&1
    rm -f crontab.txt >/dev/null 2>&1

    if [[ ${PACKAGE_MANAGER} == 'dnf' || ${PACKAGE_MANAGER} == 'yum' ]];then
        systemctl restart crond >/dev/null 2>&1
    else
        systemctl restart cron >/dev/null 2>&1
    fi

    #删除multi-v2ray环境变量
    sed -i '/v2ray/d' ~/$ENV_FILE
    sed -i '/xray/d' ~/$ENV_FILE
    source ~/$ENV_FILE

    RC_SERVICE=`systemctl status rc-local|grep loaded|egrep -o "[A-Za-z/]+/rc-local.service"`

    RC_FILE=`cat $RC_SERVICE|grep ExecStart|awk '{print $1}'|cut -d = -f2`

    sed -i '/iptables/d' ~/$RC_FILE

    colorEcho ${GREEN} "uninstall success!"
}

closeSELinux() {
    #禁用SELinux
    if [ -s /etc/selinux/config ] && grep 'SELINUX=enforcing' /etc/selinux/config; then
        sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
        setenforce 0
    fi
}

checkSys() {
    #检查是否为Root
    [ $(id -u) != "0" ] && { colorEcho ${RED} "Error: Porfavor ejecute este SCRIP como root"; exit 1; }

    if [[ `command -v apt-get` ]];then
        PACKAGE_MANAGER='apt-get'
    elif [[ `command -v dnf` ]];then
        PACKAGE_MANAGER='dnf'
    elif [[ `command -v yum` ]];then
        PACKAGE_MANAGER='yum'
    else
        colorEcho $RED "Sistema No Soportado!"
        exit 1
    fi
}

#安装依赖
installDependent(){
    if [[ ${PACKAGE_MANAGER} == 'dnf' || ${PACKAGE_MANAGER} == 'yum' ]];then
        ${PACKAGE_MANAGER} install socat crontabs bash-completion which -y
    else
        ${PACKAGE_MANAGER} update
        ${PACKAGE_MANAGER} install socat cron bash-completion ntpdate gawk -y
    fi

    #install python3 & pip
    source <(curl -sL https://python3.netlify.app/install.sh)
}

updateProject() {
    [[ ! $(type pip 2>/dev/null) ]] && colorEcho $RED "pip no install!" && exit 1

    [[ -e /etc/profile.d/iptables.sh ]] && rm -f /etc/profile.d/iptables.sh

    RC_SERVICE=`systemctl status rc-local|grep loaded|egrep -o "[A-Za-z/]+/rc-local.service"`

    RC_FILE=`cat $RC_SERVICE|grep ExecStart|awk '{print $1}'|cut -d = -f2`

    if [[ ! -e $RC_FILE || -z `cat $RC_FILE|grep iptables` ]];then
        LOCAL_IP=`curl -s http://api.ipify.org 2>/dev/null`
        [[ `echo $LOCAL_IP|grep :` ]] && IPTABLE_WAY="ip6tables" || IPTABLE_WAY="iptables" 
        if [[ ! -e $RC_FILE || -z `cat $RC_FILE|grep "/bin/bash"` ]];then
            echo "#!/bin/bash" >> $RC_FILE
        fi
        if [[ -z `cat $RC_SERVICE|grep "\[Install\]"` ]];then
            cat >> $RC_SERVICE << EOF

[Install]
WantedBy=multi-user.target
EOF
            systemctl daemon-reload
        fi
        echo "[[ -e /root/.iptables ]] && $IPTABLE_WAY-restore -c < /root/.iptables" >> $RC_FILE
        chmod +x $RC_FILE
        systemctl restart rc-local
        systemctl enable rc-local

        $IPTABLE_WAY-save -c > /root/.iptables
    fi

    pip install -U v2ray_util

    if [[ -e $UTIL_PATH ]];then
        [[ -z $(cat $UTIL_PATH|grep lang) ]] && echo "lang=en" >> $UTIL_PATH
    else
        mkdir -p /etc/v2ray_util
        curl $UTIL_CFG > $UTIL_PATH
    fi

    [[ $CHINESE == 1 ]] && sed -i "s/lang=en/lang=zh/g" $UTIL_PATH

    rm -f /usr/local/bin/v2ray >/dev/null 2>&1
    ln -s $(which v2ray-util) /usr/local/bin/v2ray
    rm -f /usr/local/bin/xray >/dev/null 2>&1
    ln -s $(which v2ray-util) /usr/local/bin/xray

    #移除旧的v2ray bash_completion脚本
    [[ -e /etc/bash_completion.d/v2ray.bash ]] && rm -f /etc/bash_completion.d/v2ray.bash
    [[ -e /usr/share/bash-completion/completions/v2ray.bash ]] && rm -f /usr/share/bash-completion/completions/v2ray.bash

    #更新v2ray bash_completion脚本
    curl $BASH_COMPLETION_SHELL > /usr/share/bash-completion/completions/v2ray
    curl $BASH_COMPLETION_SHELL > /usr/share/bash-completion/completions/xray
    if [[ -z $(echo $SHELL|grep zsh) ]];then
        source /usr/share/bash-completion/completions/v2ray
        source /usr/share/bash-completion/completions/xray
    fi
    
    #安装V2ray主程序
    [[ ${INSTALL_WAY} == 0 ]] && bash <(curl -L -s https://multi.netlify.app/go.sh) --version v4.45.2
}

#时间同步
timeSync() {
    if [[ ${INSTALL_WAY} == 0 ]];then
        echo -e "${Info} Sincronizando tiempo!.. ${Font}"
        if [[ `command -v ntpdate` ]];then
            ntpdate pool.ntp.org
        elif [[ `command -v chronyc` ]];then
            chronyc -a makestep
        fi

        if [[ $? -eq 0 ]];then 
            echo -e "${OK} Tiempo Sync Exitosamente ${Font}"
            echo -e "${OK} Ahora: `date -R`${Font}"
        fi
    fi
}

profileInit() {

    #清理v2ray模块环境变量
    [[ $(grep v2ray ~/$ENV_FILE) ]] && sed -i '/v2ray/d' ~/$ENV_FILE && source ~/$ENV_FILE

    #解决Python3中文显示问题
    [[ -z $(grep PYTHONIOENCODING=utf-8 ~/$ENV_FILE) ]] && echo "export PYTHONIOENCODING=utf-8" >> ~/$ENV_FILE && source ~/$ENV_FILE

    #全新安装的新配置
    [[ ${INSTALL_WAY} == 0 ]] && v2ray new || v2ray new

    echo ""
}

installFinish() {
#if [[ ${INSTALL_WAY} == 0 ]]; then
clear&&clear
#v2ray new
#v2ray info
#echo -e "porfavor dijite 'v2ray' para administrar v2ray\n"
#fi
	config='/etc/v2ray/config.json'
    tmp='/etc/v2ray/temp.json'
    #jq 'del(.inbounds[].streamSettings.kcpSettings[])' < $config >> $tmp
    #rm -rf /etc/v2ray/config.json
    #jq '.inbounds[].streamSettings += {"network":"ws","wsSettings":{"path": "/ADMcgh/","headers": {"Host": "ejemplo.com"}}}' < $tmp >> $config
    chmod 777 $config
    msg -bar3
    if [[ $(v2ray restart|grep success) ]]; then
    	[[ $(which v2ray) ]] && v2ray info
    	msg -bar3
        echo -e "\033[1;32mINSTALACION FINALIZADA"
    else
    	[[ $(which v2ray) ]] && v2ray info
    	msg -bar3
        print_center -verm2 "INSTALACION FINALIZADA"
        echo -e "\033[1;31m "  'Pero fallo el reinicio del servicio v2ray'
	echo -e " LEA DETALLADAMENTE LOS MENSAJES "
	echo -e ""
    fi
	cd ${BEGIN_PATH}
    [[ ${INSTALL_WAY} == 0 ]] && WAY="install" || WAY="update"
    colorEcho  ${GREEN} "multi-v2ray ${WAY} success!\n"
    echo -e  "Por favor verifique el log"
    read -p " presiona enter"
	#chekKEY &> /dev/null 2>&1
    #回到原点
}


main() {

    [[ ${HELP} == 1 ]] && help && return

    [[ ${REMOVE} == 1 ]] && colorEcho ${BLUE} " REMOVE BY @drowkid01 " && removeV2Ray && return

    [[ ${INSTALL_WAY} == 0 ]] && colorEcho ${BLUE} " INSTALACION NUEVA NATIVA By @drowkid01\n"

    checkSys

    installDependent

    closeSELinux

    timeSync

    updateProject

    profileInit

    installFinish
}

main
}

function v2ray_s(){
clear&&clear
funINIT() {
[[ $1 == 1 ]] && {
echo 'source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/v2ray1.sh)' > /bin/menuv2ray
chmod +x /bin/menuv2ray 
echo -e " RECUERDA QUE PARA UN INICIO RAPIDO SOLO DIGITA"
msg -bar3
print_center -verm2 "menuv2ray"
msg -bar3
msg -ne "Enter Para Continuar" && read enter
echo "v2ray" >> /etc/checkV
				} 
[[ $1 == 2 ]] && {
[[ $(cat /etc/checkV | wc -l) > 4 ]] && return
echo -e " RECUERDA QUE PARA UN INICIO RAPIDO SOLO DIJITA"
msg -bar3
print_center -verm2 "menuv2ray"
msg -bar3
msg -ne "Enter Para Continuar" && read enter
echo "v2ray" >> /etc/checkV
				}
}
blanco(){
	[[ !  $2 = 0 ]] && {
		echo -e "\033[1;37m$1\033[0m"
	} || {
		echo -ne " \033[1;37m$1:\033[0m "
	}
}

verde(){
	[[ !  $2 = 0 ]] && {
		echo -e "\033[1;32m$1\033[0m"
	} || {
		echo -ne " \033[1;32m$1:\033[0m "
	}
}

rojo(){
	[[ !  $2 = 0 ]] && {
		echo -e "\033[1;31m$1\033[0m"
	} || {
		echo -ne " \033[1;31m$1:\033[0m "
	}
}

col(){

	nom=$(printf '%-55s' "\033[0;92m${1} \033[0;31m>> \033[1;37m${2}")
	echo -e "	$nom\033[0;31m${3}   \033[0;92m${4}\033[0m"
}

col2(){

	echo -e " \033[1;91m$1\033[0m \033[1;37m$2\033[0m"
}

vacio(){

	blanco "\n no se puede ingresar campos vacios..."
}

cancelar(){

	echo -e "\n \033[3;49;31minstalacion cancelada...\033[0m"
}

continuar(){

	echo -e " \033[3;49;32mEnter para continuar...\033[0m"
}

title2(){
v2rayports=`lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN" | grep v2ray | awk '{print substr($9,3); }' > /tmp/v2ray.txt && echo | cat /tmp/v2ray.txt | tr '\n' ' ' > ${sdir[0]}/v2rayports.txt && cat ${sdir[0]}/v2rayports.txt` > /dev/null 2>&1 
v2rayports=$(echo $v2rayports | awk {'print $1'})
_tconex=$(netstat -nap | grep "$v2rayports" | grep v2ray | grep ESTABLISHED| grep tcp6 | awk {'print $5'} | awk -F ":" '{print $1}' | sort | uniq | wc -l)
	v1="v1.2"
	v2=$v1
	msg -bar3
	[[ $v1 = $v2 ]] && echo -e "   \e[97m\033[1;41m V2ray by @Rufu99 Remasterizado @drowkid01 [$v1] \033[0m" || echo -e " \e[97m\033[1;41m V2ray by @Rufu99 Remasterizado @drowkid01 [$v1] >> \033[1;92m[$v2] \033[0m"
[[ ! -z $v2rayports ]] && echo -e "       \e[97m\033[1;41mPUERTO ACTIVO :\033[0m \033[3;32m$v2rayports\033[0m   \e[97m\033[1;41m ACTIVOS:\033[0m \033[3;32m\e[97m\033[1;41m $_tconex " ||  echo -e "  \e[97m\033[1;41mERROR A INICIAR V2RAY : \033[0m \033[3;32m FAIL\033[3;32m"
	}

title(){
	msg -bar3
	blanco "$1"
	msg -bar3
}

userDat(){
	blanco "	N°    Usuarios 		  fech exp   dias"
	msg -bar3
}


[[ ! -e /bin/menuv2ray ]] && funINIT "1" || funINIT "2"
clear&&clear
install_ini () {
sudo apt-get install software-properties-common -y
add-apt-repository universe
apt update -y; apt upgrade -y
clear
echo -e "$BARRA"
echo -e "\033[92m        -- INSTALANDO PAQUETES NECESARIOS -- "
echo -e "$BARRA"
#bc
[[ $(dpkg --get-selections|grep -w "bc"|head -1) ]] || apt-get install bc -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "bc"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "bc"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install bc................... $ESTATUS "
#jq
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] || apt-get install jq -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install jq................... $ESTATUS "
#python
[[ $(dpkg --get-selections|grep -w "python"|head -1) ]] || apt-get install python -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "python"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "python"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install python............... $ESTATUS "
#pip
[[ $(dpkg --get-selections|grep -w "python-pip"|head -1) ]] || apt-get install python-pip -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "python-pip"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "python-pip"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install python-pip........... $ESTATUS "
#python3
[[ $(dpkg --get-selections|grep -w "python3"|head -1) ]] || apt-get install python3 -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "python3"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "python3"|head -1) ]] && ESTATUS=`echo -e "\e[3;32mINSTALADO\e[0m"` &>/dev/null
echo -e "\033[97m  # apt-get install python3.............. $ESTATUS "
#python3-pip
[[ $(dpkg --get-selections|grep -w "python3-pip"|head -1) ]] || apt-get install python3-pip -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "python3-pip"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "python3-pip"|head -1) ]] && ESTATUS=`echo -e "\e[3;32mINSTALADO\e[0m"` &>/dev/null
echo -e "\033[97m  # apt-get install python3-pip.......... $ESTATUS "
#curl
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] || apt-get install curl -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install curl................. $ESTATUS "
#npm
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] || apt-get install npm -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install npm.................. $ESTATUS "
#nodejs
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] || apt-get install nodejs -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install nodejs............... $ESTATUS "
#socat
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] || apt-get install socat -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install socat................ $ESTATUS "
#netcat
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] || apt-get install netcat -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install netcat............... $ESTATUS "
#netcat-traditional
[[ $(dpkg --get-selections|grep -w "netcat-traditional"|head -1) ]] || apt-get install netcat-traditional -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat-traditional"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat-traditional"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install netcat-traditional... $ESTATUS "
#net-tools
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] || apt-get net-tools -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install net-tools............ $ESTATUS "
#cowsay
[[ $(dpkg --get-selections|grep -w "cowsay"|head -1) ]] || apt-get install cowsay -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "cowsay"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "cowsay"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install cowsay............... $ESTATUS "
#figlet
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] || apt-get install figlet -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install figlet............... $ESTATUS "
#lolcat
apt-get install lolcat -y &>/dev/null
sudo gem install lolcat &>/dev/null
[[ $(dpkg --get-selections|grep -w "lolcat"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "lolcat"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install lolcat............... $ESTATUS "

echo -e "$BARRA"
echo -e "\033[92m La instalacion de paquetes necesarios a finalizado"
echo -e "$BARRA"
echo -e "\033[97m Si la instalacion de paquetes tiene fallas"
echo -ne "\033[97m Puede intentar de nuevo [s/n]: "
read inst
[[ $inst = @(s|S|y|Y) ]] && install_ini
}


SCPdir="${sdir[0]}"
SCPfrm="${SCPdir}/herramientas" && [[ ! -d ${SCPfrm} ]]
[[ ! -d ${SCPfrm} ]] && mkdir ${SCPfrm}
SCPinst="${SCPdir}/protocolos" && [[ ! -d ${SCPinst} ]] 
[[ ! -d ${SCPinst} ]] && mkdir ${SCPinst}
user_conf="${sdir[0]}/RegV2ray" && [[ ! -e $user_conf ]] && touch $user_conf
user_confEX="${sdir[0]}/RegV2ray.exp" && [[ ! -e $user_confEX ]] && touch $user_confEX
config="/etc/v2ray/config.json"
temp="/etc/v2ray/temp.json"
err_fun () {
     case $1 in
     1)msg -verm "-Usuario Nulo-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     2)msg -verm "-Nombre muy corto (MIN: 2 CARACTERES)-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     3)msg -verm "-Nombre muy grande (MAX: 5 CARACTERES)-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     4)msg -verm "-Contraseña Nula-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     5)msg -verm "-Contraseña muy corta-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     6)msg -verm "-Contraseña muy grande-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     7)msg -verm "-Duracion Nula-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     8)msg -verm "-Duracion invalida utilize numeros-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     9)msg -verm "-Duracion maxima y de un año-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     11)msg -verm "-Limite Nulo-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     12)msg -verm "-Limite invalido utilize numeros-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     13)msg -verm "-Limite maximo de 999-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     14)msg -verm "-Usuario Ya Existe-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
	 15)msg -verm "-(Solo numeros) GB = Min: 1gb Max: 1000gb-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
	 16)msg -verm "-(Solo numeros)-"; sleep 2s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
	 17)msg -verm "-(Sin Informacion - Para Cancelar Digite CRTL + C)-"; sleep 4s; tput cuu1; tput dl1; tput cuu1; tput dl1;;
     esac
}
intallv2ray () {
install_ini
source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/menu_inst/v2ray.sh)
#source <(curl -sL https://raw.githubusercontent.com/@drowkid01/ADMRufu/main/Utils/v2ray/v2ray.sh)
[[ -e "$config" ]] && jq 'del(.inbounds[].streamSettings.kcpSettings[])' < /etc/v2ray/config.json >> /etc/v2ray/tmp.json
#rm -rf /etc/v2ray/config.json
#[[ -e "$config" ]] && jq '.inbounds[].streamSettings += {"network":"ws","wsSettings":{"path": "/ADMcgh/","headers": {"Host": "ejemplo.com"}}}' < /etc/v2ray/tmp.json >> /etc/v2ray/config.json
#v2ray new
[[ -e "$config" ]] && chmod 777 /etc/v2ray/config.json
echo '[Unit]
Description=V2Ray Service
After=network.target nss-lookup.target
StartLimitIntervalSec=0

[Service]
Type=simple
User=root
CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
AmbientCapabilities=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
NoNewPrivileges=true
ExecStart=/usr/bin/v2ray/v2ray -config /etc/v2ray/config.json
Restart=always
RestartSec=3s


[Install]
WantedBy=multi-user.target' > /etc/systemd/system/v2ray.service
systemctl daemon-reload &>/dev/null
systemctl start v2ray &>/dev/null
systemctl enable v2ray &>/dev/null
systemctl restart v2ray.service
msg -ama "-Intalado con Exito-!"
USRdatabase="${sdir[0]}/RegV2ray"
[[ ! -e ${USRdatabase} ]] && touch ${USRdatabase}
sort ${USRdatabase} | uniq > ${USRdatabase}tmp
mv -f ${USRdatabase}tmp ${USRdatabase}
msg -bar3
msg -ne "Enter Para Continuar" && read enter
[[ ! -d ${SCPinst} ]] && mkdir ${SCPinst}
[[ ! -d ${sdir[0]}/v2ray ]] && mkdir ${sdir[0]}/v2ray
echo 'source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/v2ray1.sh)' > /bin/menuv2ray
chmod +x /bin/menuv2ray
}
protocolv2ray () {
msg -ama "-Escojer opcion 3 y poner el dominio de nuestra IP-!"
msg -bar3
v2ray stream
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
dirapache="/usr/local/lib/ubuntn/apache/ver" && [[ ! -d ${dirapache} ]]
tls () {
msg -ama "-Activar o Desactivar TLS-!"
#msg -bar3
#echo -e "Ingrese Correo Temporal o Fijo \n  Para Validar su Cerficicado SSL " 
#read -p " Ejemplo email=my@example.com : " crreo
#echo -e $barra
#wget -O -  https://get.acme.sh | sh -s email=$crreo
msg -bar3
v2ray tls
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
portv () {
msg -ama "-Cambiar Puerto v2ray-!"
msg -bar3
v2ray port
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
stats () {
#msg -ama "-Estadisticas de Consumo-!"
#msg -bar3
#v2ray stats
usrCONEC
#msg -bar3
#msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
unistallv2 () {
source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/menu_inst/v2ray.sh) --remove > /dev/null 2>&1
#source <(curl -sSL https://www.dropbox.com/s/cx8xhq3s53x3a75/insta-gen.sh)
rm -rf ${sdir[0]}/RegV2ray > /dev/null 2>&1
echo -e "\033[1;92m                  V2RAY REMOVIDO OK "
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
infocuenta () {
v2ray info
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
addusr () {
clear 
clear
msg -bar3
msg -tit
msg -ama "             AGREGAR USUARIO | UUID V2RAY"
msg -bar3
echo ""
while true; do
echo -ne "\e[91m >> Digita un Nombre: \033[1;92m"
     read -p ": " nick
     nick="$(echo $nick|sed -e 's/[^a-z0-9 -]//ig')"
     if [[ -z $nick ]]; then
     err_fun 17 && continue
     elif [[ "${#nick}" -lt "1" ]]; then
     err_fun 2 && continue
     elif [[ "${#nick}" -gt "9" ]]; then
     err_fun 3 && continue
     fi
     break
done
##DAIS
valid=$(date '+%C%y-%m-%d' -d " +31 days")		  
##CORREO		  
#MAILITO=$(cat /dev/urandom | tr -dc '[:alnum:]' | head -c 10)
MAILITO="${nick}"
##ADDUSERV2RAY		  
UUID=`uuidgen`	  
sed -i '13i\           \{' /etc/v2ray/config.json
sed -i '14i\           \"alterId": 0,' /etc/v2ray/config.json
sed -i '15i\           \"id": "'$UUID'",' /etc/v2ray/config.json
sed -i '16i\           \"email": "'$MAILITO'"' /etc/v2ray/config.json
sed -i '17i\           \},' /etc/v2ray/config.json
echo -e "\e[91m >> Agregado UUID: \e[92m$UUID "
while true; do
     echo -ne "\e[91m >> Duracion de UUID (Dias):\033[1;92m " && read diasuser
     if [[ -z "$diasuser" ]]; then
     err_fun 17 && continue
     elif [[ "$diasuser" != +([0-9]) ]]; then
     err_fun 8 && continue
     elif [[ "$diasuser" -gt "360" ]]; then
     err_fun 9 && continue
     fi 
     break
done
#Lim
[[ $(cat /etc/passwd |grep $1: |grep -vi [a-z]$1 |grep -v [0-9]$1 > /dev/null) ]] && return 1
valid=$(date '+%C%y-%m-%d' -d " +$diasuser days") && datexp=$(date "+%F" -d " + $diasuser days")
echo -e "\e[91m >> Expira el : \e[92m$datexp "
##Registro
echo "  $UUID | $nick | $valid " >> ${sdir[0]}/RegV2ray
v2ray restart > /dev/null 2>&1
echo ""
v2ray info > ${sdir[0]}/v2ray/confuuid.log
lineP=$(sed -n '/'${UUID}'/=' ${sdir[0]}/v2ray/confuuid.log)
numl1=4
let suma=$lineP+$numl1
sed -n ${suma}p ${sdir[0]}/v2ray/confuuid.log 
echo ""
msg -bar3
echo -e "\e[92m           UUID AGREGEGADO CON EXITO "
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}

add_user(){
	unset seg
	seg=$(date +%s)
	while :
	do
	clear
	users="$(cat $config | jq -r .inbounds[].settings.clients[].email)"

	title "		CREAR USUARIO V2RAY"
	userDat

	n=0
	for i in $users
	do
		unset DateExp
		unset seg_exp
		unset exp

		[[ $i = null ]] && {
			i="default"
			a='*'
			DateExp=" unlimit"
			col "$a)" "$i" "$DateExp"
		} || {
			DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
			seg_exp=$(date +%s --date="$DateExp")
			exp="[$(($(($seg_exp - $seg)) / 86400))]"

			col "$n)" "$i" "$DateExp" "$exp"
		}
		let n++
	done
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco "NOMBRE DEL NUEVO USUARIO" 0
	read opcion

	[[ -z $opcion ]] && vacio && sleep 0.3 && continue

	[[ $opcion = 0 ]] && break

	blanco "DURACION EN DIAS" 0
	read dias

	espacios=$(echo "$opcion" | tr -d '[[:space:]]')
	opcion=$espacios

	mv $config $temp
	num=$(jq '.inbounds[].settings.clients | length' $temp)
	new=".inbounds[].settings.clients[$num]"
	new_id=$(uuidgen)
	new_mail="email:\"$opcion\""
	aid=$(jq '.inbounds[].settings.clients[0].alterId' $temp)
	echo jq \'$new += \{alterId:${aid},id:\"$new_id\","$new_mail"\}\' $temp \> $config | bash
	echo "$opcion | $new_id | $(date '+%y-%m-%d' -d " +$dias days")" >> $user_conf
	chmod 777 $config
	rm $temp
	clear
	msg -bar3
	blanco "	Usuario $opcion creado Exitosamente"
	msg -bar3
	restart_v2r
	sleep 0.2
    done
}


usrCONEC() {
CGHlog='/var/log/v2ray/access.log'
[[ $log0 -le 1 ]] && {
v2ray restart &> /dev/null
v2ray clean &> /dev/null && let log0++ && clear 
}
msg -bar3
echo -e ""
echo -e " ESPERANDO A LA VERIFICACION DE IPS Y USUARIOS "
echo -e "      ESPERE UN MOMENTO PORFAVOR $log0"
echo -e ""
msg -bar3
fun_bar
msg -bar3
sleep 5s
clear&&clear
title2
msg -bar3
users="$(cat $config | jq -r .inbounds[].settings.clients[].email)"
IP_tconex=$(netstat -nap | grep "$v2rayports"| grep v2ray | grep ESTABLISHED | grep tcp6 | awk {'print $5'}| awk -F ":" '{print $1}' | sort | uniq)
n=1
[[ -z $IP_tconex ]] && echo -e " NO HAY USUARIOS CONECTADOS!"
for i in $IP_tconex
do
	USERauth=$(cat ${CGHlog} | grep $i | grep accepted |awk '{print $7}'| sort | uniq)
	Users+="$USERauth\n"
done
echo -e " N) -|- USER -|- CONEXIONES "|column -t -s '-'
msg -bar3
for U in $users
	do
	CConT=$(echo -e "$Users" | grep $U |wc -l)
	[[ $CConT = 0 ]] && continue
	UConc+=" $n) -|- $U -|- $CConT\n"
	let n++
done
echo -e "$UConc"|column -t -s '-'
msg -bar3
continuar
read foo
}


renewusr () {
clear 
clear
msg -bar3
msg -tit
msg -ama "         USUARIOS REGISTRADOS | UUID V2RAY"
msg -bar3
# usersss=$(cat ${sdir[0]}/RegV2ray|cut -d '|' -f1)
# cat ${sdir[0]}/RegV2ray|cut -d'|' -f3
VPSsec=$(date +%s)
local HOST="${sdir[0]}/RegV2ray.exp"
local HOST2="${sdir[0]}/RegV2ray.exp"
local RETURN="$(cat $HOST|cut -d'|' -f2)"
local IDEUUID="$(cat $HOST|cut -d'|' -f1)"
if [[ -z $RETURN ]]; then
echo -e "----- NINGUN USER EXPIRADO -----"
msg -ne "Enter Para Continuar" && read enter
menuv2ray
else
i=1
echo -e "\e[97m                 UUID                | USER | DATA \e[93m"
msg -bar3
while read hostreturn ; do
DateExp="$(cat ${sdir[0]}/RegV2ray.exp|grep -w "$hostreturn"|cut -d'|' -f3)"
if [[ ! -z $DateExp ]]; then             
DataSec=$(date +%s --date="$DateExp")
[[ "$VPSsec" -gt "$DataSec" ]] && EXPTIME="\e[91m[Caducado]\e[97m" || EXPTIME="\e[92m[$(($(($DataSec - $VPSsec)) / 86400))]\e[97m Dias"
else
EXPTIME="\e[91m[ S/R ]"
fi 
usris="$(cat ${sdir[0]}/RegV2ray.exp|grep -w "$hostreturn"|cut -d'|' -f2)"
local contador_secuencial+="\e[93m$hostreturn \e[97m|\e[93m$usris\e[97m|\e[93m $EXPTIME \n"           
      if [[ $i -gt 30 ]]; then
	      echo -e "$contador_secuencial"
	  unset contador_secuencial
	  unset i
	  fi
let i++
done <<< "$IDEUUID"

[[ ! -z $contador_secuencial ]] && {
linesss=$(cat ${sdir[0]}/RegV2ray.exp | wc -l)
	      echo -e "$contador_secuencial \n Numero de Registrados: $linesss"
	}
fi

read -p "" 
return
msg -bar3
msg -tit
msg -ama "             AGREGAR USUARIO | UUID V2RAY"
msg -bar3
##DAIS
valid=$(date '+%C%y-%m-%d' -d " +31 days")		  
##CORREO		  
MAILITO=$(cat /dev/urandom | tr -dc '[:alnum:]' | head -c 10)
##ADDUSERV2RAY		  
UUID=`uuidgen`	  
sed -i '13i\           \{' /etc/v2ray/config.json
sed -i '14i\           \"alterId": 0,' /etc/v2ray/config.json
sed -i '15i\           \"id": "'$UUID'",' /etc/v2ray/config.json
sed -i '16i\           \"email": "'$MAILITO'@gmail.com"' /etc/v2ray/config.json
sed -i '17i\           \},' /etc/v2ray/config.json
echo ""
echo -e "\e[91m >> Agregado UUID: \e[92m$UUID "
while true; do
     echo -ne "\e[91m >> Duracion de UUID (Dias):\033[1;92m " && read diasuser
     if [[ -z "$diasuser" ]]; then
     err_fun 17 && continue
     elif [[ "$diasuser" != +([0-9]) ]]; then
     err_fun 8 && continue
     elif [[ "$diasuser" -gt "360" ]]; then
     err_fun 9 && continue
     fi 
     break
done
#Lim
[[ $(cat /etc/passwd |grep $1: |grep -vi [a-z]$1 |grep -v [0-9]$1 > /dev/null) ]] && return 1
valid=$(date '+%C%y-%m-%d' -d " +$diasuser days") && datexp=$(date "+%F" -d " + $diasuser days")
echo -e "\e[91m >> Expira el : \e[92m$datexp "
##Registro
echo "  $UUID | $nick | $valid " >> ${sdir[0]}/RegV2ray
v2ray restart > /dev/null 2>&1
echo ""
v2ray info > ${sdir[0]}/v2ray/confuuid.log
lineP=$(sed -n '/'${UUID}'/=' ${sdir[0]}/v2ray/confuuid.log)
numl1=4
let suma=$lineP+$numl1
sed -n ${suma}p ${sdir[0]}/v2ray/confuuid.log 
echo ""
msg -bar3
echo -e "\e[92m           UUID AGREGEGADO CON EXITO "
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}


renew(){
	while :
	do
		unset user
		clear
		title "		RENOVAR USUARIOS"
		userDat
		userEpx=$(cut -d " " -f1 $user_conf)
		n=1
		for i in $userEpx
		do
			DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
			seg_exp=$(date +%s --date="$DateExp")
			[[ "$seg" -gt "$seg_exp" ]] && {
				col "$n)" "$i" "$DateExp" "\033[0;31m[Exp]"
				uid[$n]="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f2|tr -d '[[:space:]]')"
				user[$n]=$i
				let n++
			}
		done
		[[ -z ${user[1]} ]] && blanco "		No hay expirados"
		msg -bar3
		col "0)" "VOLVER"
		msg -bar3
		blanco "NUMERO DE USUARIO A RENOVAR" 0
		read opcion

		[[ -z $opcion ]] && vacio && sleep 0.3 && continue
		[[ $opcion = 0 ]] && break

		[[ ! $opcion =~ $numero ]] && {
			blanco " solo numeros apartir de 1"
			sleep 0.2
		} || {
			[[ $opcion>=${n} ]] && {
				let n--
				blanco "solo numero entre 1 y $n"
				sleep 0.2
		} || {
			blanco "DURACION EN DIAS" 0
			read dias

			mv $config $temp
			num=$(jq '.inbounds[].settings.clients | length' $temp)
			aid=$(jq '.inbounds[].settings.clients[0].alterId' $temp)
			echo "cat $temp | jq '.inbounds[].settings.clients[$num] += {alterId:${aid},id:\"${uid[$opcion]}\",email:\"${user[$opcion]}\"}' >> $config" | bash
			sed -i "/${user[$opcion]}/d" $user_conf
			echo "${user[$opcion]} | ${uid[$opcion]} | $(date '+%y-%m-%d' -d " +$dias days")" >> $user_conf
			chmod 777 $config
			rm $temp
			clear
			msg -bar3
			blanco "	Usuario ${user[$opcion]} renovado Exitosamente"
			msg -bar3
			restart_v2r
			sleep 0.2
		  }
		}
	done
}



delusr () {
clear 
clear
invaliduuid () {
msg -bar3
echo -e "\e[91m                    UUID INVALIDO \n$(msg -bar3)"
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
msg -bar3
msg -tit
msg -ama "             ELIMINAR USUARIO | UUID V2RAY"
msg -bar3
echo -e "\e[97m               USUARIOS REGISTRADOS"
echo -e "\e[33m$(cat ${sdir[0]}/RegV2ray|cut -d '|' -f2,1)" 
msg -bar3
echo -ne "\e[91m >> Digita el UUID a elininar:\n \033[1;92m " && read uuidel
[[ $(sed -n '/'${uuidel}'/=' /etc/v2ray/config.json|head -1) ]] || invaliduuid
lineP=$(sed -n '/'${uuidel}'/=' /etc/v2ray/config.json)
linePre=$(sed -n '/'${uuidel}'/=' ${sdir[0]}/RegV2ray)
sed -i "${linePre}d" ${sdir[0]}/RegV2ray
numl1=2
let resta=$lineP-$numl1
sed -i "${resta}d" /etc/v2ray/config.json
sed -i "${resta}d" /etc/v2ray/config.json
sed -i "${resta}d" /etc/v2ray/config.json
sed -i "${resta}d" /etc/v2ray/config.json
sed -i "${resta}d" /etc/v2ray/config.json
v2ray restart > /dev/null 2>&1
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}

dell_user(){
	unset seg
	seg=$(date +%s)
	while :
	do
	clear
	users=$(cat $config | jq .inbounds[].settings.clients[] | jq -r .email)

	title "	ELIMINAR USUARIO V2RAY"
	userDat
	n=0
	for i in $users
	do
		userd[$n]=$i
		unset DateExp
		unset seg_exp
		unset exp

		[[ $i = null ]] && {
			i="default"
			a='*'
			DateExp=" unlimit"
			col "$a)" "$i" "$DateExp"
		} || {
			DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
			seg_exp=$(date +%s --date="$DateExp")
			exp="[$(($(($seg_exp - $seg)) / 86400))]"
			col "$n)" "$i" "$DateExp" "$exp"
		}
		p=$n
		let n++
	done
	userEpx=$(cut -d " " -f 1 $user_conf)
	for i in $userEpx
	do	
		DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
		seg_exp=$(date +%s --date="$DateExp")
		[[ "$seg" -gt "$seg_exp" ]] && {
			col "$n)" "$i" "$DateExp" "\033[0;31m[Exp]"
			expUser[$n]=$i
		}
		let n++
	done
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco "NUMERO DE USUARIO A ELIMINAR" 0
	read opcion

	[[ -z $opcion ]] && vacio && sleep 0.3 && continue
	[[ $opcion = 0 ]] && break

	[[ ! $opcion =~ $numero ]] && {
		blanco " solo numeros apartir de 1"
		sleep 0.2
	} || {
		let n--
		[[ $opcion>=${n} ]] && {
			blanco "solo numero entre 1 y $n"
			sleep 0.2
		} || {
			[[ $opcion>=${p} ]] && {
				sed -i "/${expUser[$opcion]}/d" $user_conf
			} || {
			sed -i "/${userd[$opcion]}/d" $user_conf
			mv $config $temp
			echo jq \'del\(.inbounds[].settings.clients[$opcion]\)\' $temp \> $config | bash
			chmod 777 $config
			rm $temp
			clear
			msg -bar3
			blanco "	Usuario eliminado"
			msg -bar3
			v2ray restart & > /dev/null
			}
			sleep 0.2
		}
	}
	done
}

_delUSR () {
clear&&clear
msg -bar3
echo -e "\033[0;35m[\033[0;36m1\033[0;35m] \033[0;34m<\033[0;33m  BORRAR POR UUID DE USUARIO\033[1;32m "
echo -e "\033[0;35m[\033[0;36m2\033[0;35m] \033[0;34m<\033[0;33m  BORRAR POR NUMERO DE USUARIO \033[1;32m "
msg -bar3
selection=$(selection_fun 2)
case ${selection} in
1)dell_user;;
2)delusr;;
*)return;;
esac
return
}

autoDel(){
	seg=$(date +%s)
	while :
	do
		unset users
		users=$(cat $config | jq .inbounds[].settings.clients[] | jq -r .email)
		n=0
		for i in $users
		do
			[[ ! $i = null ]] && {
				DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
				seg_exp=$(date +%s --date="$DateExp")
				[[ "$seg" -gt "$seg_exp" ]] && {
				    #echo "$(cat ${user_conf}|grep -w "${i}")" >> ${user_conf}.exp
					#sed -i "/${i}/d" $user_conf
					mv $config $temp
					echo jq \'del\(.inbounds[].settings.clients[$n]\)\' $temp \> $config | bash
					chmod 777 $config
					rm $temp
					continue
				}
			}
			let n++
			done
			break
		done
#v2ray restart
	}

mosusr_kk() {
clear 
clear
msg -bar3
msg -tit
msg -ama "         USUARIOS REGISTRADOS | UUID V2RAY"
msg -bar3
# usersss=$(cat ${sdir[0]}/RegV2ray|cut -d '|' -f1)
# cat ${sdir[0]}/RegV2ray|cut -d'|' -f3
VPSsec=$(date +%s)
local HOST="${sdir[0]}/RegV2ray"
local HOST2="${sdir[0]}/RegV2ray"
local RETURN="$(cat $HOST|cut -d'|' -f2)"
local IDEUUID="$(cat $HOST|cut -d'|' -f1)"
if [[ -z $RETURN ]]; then
echo -e "----- NINGUN USER REGISTRADO -----"
msg -ne "Enter Para Continuar" && read enter
menuv2ray
else
i=1
echo -e "\e[97m                 UUID                | USER | EXPIRACION \e[93m"
msg -bar3
while read hostreturn ; do
DateExp="$(cat ${sdir[0]}/RegV2ray|grep -w "$hostreturn"|cut -d'|' -f3)"
if [[ ! -z $DateExp ]]; then             
DataSec=$(date +%s --date="$DateExp")
[[ "$VPSsec" -gt "$DataSec" ]] && EXPTIME="\e[91m[Caducado]\e[97m" || EXPTIME="\e[92m[$(($(($DataSec - $VPSsec)) / 86400))]\e[97m Dias"
else
EXPTIME="\e[91m[ S/R ]"
fi 
usris="$(cat ${sdir[0]}/RegV2ray|grep -w "$hostreturn"|cut -d'|' -f2)"
local contador_secuencial+="\e[93m$hostreturn \e[97m|\e[93m$usris\e[97m|\e[93m $EXPTIME \n"           
      if [[ $i -gt 30 ]]; then
	      echo -e "$contador_secuencial"
	  unset contador_secuencial
	  unset i
	  fi
let i++
done <<< "$IDEUUID"

[[ ! -z $contador_secuencial ]] && {
linesss=$(cat ${sdir[0]}/RegV2ray | wc -l)
	      echo -e "$contador_secuencial \n Numero de Registrados: $linesss"
	}
fi
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
lim_port () {
clear 
clear
msg -bar3
msg -tit
msg -ama "          LIMITAR MB X PORT | UUID V2RAY"
msg -bar3
###VER
estarts () {
VPSsec=$(date +%s)
local HOST="${sdir[0]}/v2ray/lisportt.log"
local HOST2="${sdir[0]}/v2ray/lisportt.log"
local RETURN="$(cat $HOST|cut -d'|' -f2)"
local IDEUUID="$(cat $HOST|cut -d'|' -f1)"
if [[ -z $RETURN ]]; then
echo -e "----- NINGUN PUERTO REGISTRADO -----"
msg -ne "Enter Para Continuar" && read enter
menuv2ray
else
i=1
while read hostreturn ; do
iptables -n -v -L > ${sdir[0]}/v2ray/data1.log 
statsss=$(cat ${sdir[0]}/v2ray/data1.log|grep -w "tcp spt:$hostreturn quota:"|cut -d' ' -f3,4,5)
gblim=$(cat ${sdir[0]}/v2ray/lisportt.log|grep -w "$hostreturn"|cut -d'|' -f2)
local contador_secuencial+="         \e[97mPUERTO: \e[93m$hostreturn \e[97m|\e[93m$statsss \e[97m|\e[93m $gblim GB  \n"          
      if [[ $i -gt 30 ]]; then
	      echo -e "$contador_secuencial"
	  unset contador_secuencial
	  unset i
	  fi
let i++
done <<< "$IDEUUID"

[[ ! -z $contador_secuencial ]] && {
linesss=$(cat ${sdir[0]}/v2ray/lisportt.log | wc -l)
	      echo -e "$contador_secuencial \n Puertos Limitados: $linesss"
	}
fi
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
###LIM
liport () {
while true; do
     echo -ne "\e[91m >> Digite Port a Limitar:\033[1;92m " && read portbg
     if [[ -z "$portbg" ]]; then
     err_fun 17 && continue
     elif [[ "$portbg" != +([0-9]) ]]; then
     err_fun 16 && continue
     elif [[ "$portbg" -gt "1000" ]]; then
     err_fun 16 && continue
     fi 
     break
done
while true; do
     echo -ne "\e[91m >> Digite Cantidad de GB:\033[1;92m " && read capgb
     if [[ -z "$capgb" ]]; then
     err_fun 17 && continue
     elif [[ "$capgb" != +([0-9]) ]]; then
     err_fun 15 && continue
     elif [[ "$capgb" -gt "1000" ]]; then
     err_fun 15 && continue
     fi 
     break
done
uml1=1073741824
gbuser="$capgb"
let multiplicacion=$uml1*$gbuser
sudo iptables -I OUTPUT -p tcp --sport $portbg -j DROP
sudo iptables -I OUTPUT -p tcp --sport $portbg -m quota --quota $multiplicacion -j ACCEPT
iptables-save > /etc/iptables/rules.v4
echo ""
echo -e " Port Seleccionado: $portbg | Cantidad de GB: $gbuser"
echo ""
echo " $portbg | $gbuser | $multiplicacion " >> ${sdir[0]}/v2ray/lisportt.log 
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
###RES
resdata () {
VPSsec=$(date +%s)
local HOST="${sdir[0]}/v2ray/lisportt.log"
local HOST2="${sdir[0]}/v2ray/lisportt.log"
local RETURN="$(cat $HOST|cut -d'|' -f2)"
local IDEUUID="$(cat $HOST|cut -d'|' -f1)"
if [[ -z $RETURN ]]; then
echo -e "----- NINGUN PUERTO REGISTRADO -----"
return 0
else
i=1
while read hostreturn ; do
iptables -n -v -L > ${sdir[0]}/v2ray/data1.log 
statsss=$(cat ${sdir[0]}/v2ray/data1.log|grep -w "tcp spt:$hostreturn quota:"|cut -d' ' -f3,4,5)
gblim=$(cat ${sdir[0]}/v2ray/lisportt.log|grep -w "$hostreturn"|cut -d'|' -f2)
local contador_secuencial+="         \e[97mPUERTO: \e[93m$hostreturn \e[97m|\e[93m$statsss \e[97m|\e[93m $gblim GB  \n"  
        
      if [[ $i -gt 30 ]]; then
	      echo -e "$contador_secuencial"
	  unset contador_secuencial
	  unset i
	  fi
let i++
done <<< "$IDEUUID"

[[ ! -z $contador_secuencial ]] && {
linesss=$(cat ${sdir[0]}/v2ray/lisportt.log | wc -l)
	      echo -e "$contador_secuencial \n Puertos Limitados: $linesss"
	}
fi
msg -bar3

while true; do
     echo -ne "\e[91m >> Digite Puerto a Limpiar:\033[1;92m " && read portbg
     if [[ -z "$portbg" ]]; then
     err_fun 17 && continue
     elif [[ "$portbg" != +([0-9]) ]]; then
     err_fun 16 && continue
     elif [[ "$portbg" -gt "1000" ]]; then
     err_fun 16 && continue
     fi 
     break
done
invaliduuid () {
msg -bar3
echo -e "\e[91m                PUERTO INVALIDO \n$(msg -bar3)"
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}
[[ $(sed -n '/'${portbg}'/=' ${sdir[0]}/v2ray/lisportt.log|head -1) ]] || invaliduuid
gblim=$(cat ${sdir[0]}/v2ray/lisportt.log|grep -w "$portbg"|cut -d'|' -f3)
sudo iptables -D OUTPUT -p tcp --sport $portbg -j DROP
sudo iptables -D OUTPUT -p tcp --sport $portbg -m quota --quota $gblim -j ACCEPT
iptables-save > /etc/iptables/rules.v4
lineP=$(sed -n '/'${portbg}'/=' ${sdir[0]}/v2ray/lisportt.log)
sed -i "${linePre}d" ${sdir[0]}/v2ray/lisportt.log
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray 
}
## MENU
echo -ne "\033[1;32m [1] > " && msg -azu "-LIMITAR DATA x PORT"
echo -ne "\033[1;32m [2] > " && msg -azu "-RESETEAR DATA DE PORT- "
echo -ne "\033[1;32m [3] > " && msg -azu "-VER DATOS CONSUMIDOS- "
echo -ne "$(msg -bar3)\n\033[1;32m [0] > " && msg -bra "\e[97m\033[1;41m VOLVER \033[1;37m"
msg -bar3
selection=$(selection_fun 3)
case ${selection} in
1)liport ;;
2)resdata;;
3)estarts;;
0)
source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/v2ray1.sh)
;;
esac
}

limpiador_activador () {
unset PIDGEN
PIDGEN=$(ps aux|grep -v grep|grep "limv2ray")
if [[ ! $PIDGEN ]]; then
screen -dmS limv2ray watch -n 21600 limv2ray
else
#killall screen
screen -S limv2ray -p 0 -X quit
fi
unset PID_GEN
PID_GEN=$(ps x|grep -v grep|grep "limv2ray")
[[ ! $PID_GEN ]] && PID_GEN="\e[91m [ DESACTIVADO ] " || PID_GEN="\e[92m [ ACTIVADO ] "
statgen="$(echo $PID_GEN)"
clear 
clear
msg -bar3
msg -tit
msg -ama "          ELIMINAR EXPIRADOS | UUID V2RAY"
msg -bar3
echo ""
echo -e "                    $statgen " 
echo "" 						
msg -bar3
msg -ne "Enter Para Continuar" && read enter
menuv2ray
}

	[[ ! -e $config ]] && {
		clear
		msg -bar3
		blanco " No se encontro ningun archivo de configracion v2ray"
		msg -bar3
		blanco "	  No instalo v2ray o esta usando\n	     una vercion diferente!!!"
		msg -bar3
		echo -e "		\033[4;31mNOTA importante\033[0m"
		echo -e " \033[0;31mSi esta usando una vercion v2ray diferente"
		echo -e " y opta por cuntinuar usando este script."
		echo -e " Este puede; no funcionar correctamente"
		echo -e " y causar problemas en futuras instalaciones.\033[0m"
		msg -bar3
msg -ne "Enter Para Continuar" && read enter
clear&&clear
	}

_fixv2() {
unset opcion
clear
	title "   restablecer ajustes v2ray"
	echo -e " \033[0;31mEsto va a restablecer los\n ajustes predeterminados de v2ray"
	echo -e " Se perdera ajuste previos,\n incluido los Usuarios\033[0m"
	echo -e "  LUEGO DE ESTO, DEBERAS RECONFIGURAR TU METODO\n\033[0m"
	msg -bar3
	blanco "quiere continuar? [S/N]" 0
	read opcion
	msg -bar3
	case $opcion in
		[Ss]|[Yy]) 
		v2ray new 
		echo "" > ${sdir[0]}/RegV2ray
		echo "" > ${sdir[0]}/RegV2ray.exp
		;;
		[Nn]) continuar && read foo;;
	esac
}

_tools(){

echo -e "         \e[97m1 CREAR: "
echo -e "         \e[97m2 RENOVAR: "
echo -e "         \e[97m3 ELIMINAR: "
echo -e "         \e[97mEstado actual: "

selection=$(selection_fun 4)
case ${selection} in
1)add_user;;
2)renew;;
3)dell_user;;
3)portv;;
esac
}

selection_fun () {
local selection="null"
local range
for((i=0; i<=$1; i++)); do range[$i]="$i "; done
while [[ ! $(echo ${range[*]}|grep -w "$selection") ]]; do
echo -ne "\033[1;37m- ► Escoje -: " >&2
read selection
tput cuu1 >&2 && tput dl1 >&2
done
echo $selection
}

[[ $1 = "autoDel" ]] && {
	autoDel
} || {
	autoDel
}
clear&&clear
[[ ! -e $config ]] && {
v2ray="\033[1;31m[OFF]"
msg -bar3
msg -tit
msg -ama "      PANNEL V2RAY Mod @drowkid01${vesaoSCT} "
msg -bar3
## INSTALADOR
echo -e "\033[0;35m[\033[0;36m1\033[0;35m] \033[0;34m<\033[0;33m  INSTALAR V2RAY         $v2ray"
echo -ne "$(msg -bar3)\n\033[1;32m [0] > " && msg -bra "\e[97m\033[1;41m VOLVER \033[1;37m"
msg -bar3
pid_inst () {
[[ $1 = "" ]] && echo -e "\033[1;31m[OFF]" && return 0
unset portas
portas_var=$(lsof -V -i -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep $1)
[[ ! -z ${portas_var} ]] && echo -e "\033[1;32m[ Servicio Activo ]" || echo -e "\033[1;31m[ Servicio Desactivado ]"
}
echo -e "         \e[97mEstado actual: $(pid_inst v2ray)"
unset selection
selection=$(selection_fun 1)
case ${selection} in
1)intallv2ray;;
esac
} || {
PID_GEN=$(ps x|grep -v grep|grep "limv2ray")
[[ ! $PID_GEN ]] && PID_GEN="\e[91m [ OFF ] " || PID_GEN="\e[92m [ ON ] "
statgen="$(echo $PID_GEN)"
[[ $(cat /etc/v2ray/config.json | jq '.inbounds[].streamSettings.security') = '"tls"' ]] && _tlsN=" \033[1;32m [ \033[0;34m ACTIVO \033[1;32m ]" || _tlsN=" \033[1;32m [ \033[0;33m CERRADO\033[1;32m ]"
[[ -e /etc/v2ray/config.json ]] && _netW="$(cat /etc/v2ray/config.json | jq '.inbounds[].streamSettings.network')" || _netW="\033[0;32mnull"
[[ -e /etc/v2ray/config.json ]] && _v2Reg="$(cat /etc/v2ray/config.json | jq .inbounds[].settings.clients[].email|wc -l)" || _v2Reg="\033[0;32mnull"
_v2RegE=$(cat $user_confEX | wc -l)
v1=$(cat /bin/ejecutar/v-new.log)
v2=$(cat ${sdir[0]}/v-local.log)
[[ $v1 = $v2 ]] && vesaoSCT="\033[0;33m ($v2)" || vesaoSCT="\033[0;33m($v2) ► \033[1;32m($v1)\033[1;31m"
v2rayports=`netstat -tunlp | grep v2ray | grep LISTEN | grep -vE '127.0.0.1' | awk '{print substr($4,4); }' > /tmp/v2.txt && echo | cat /tmp/v2.txt | tr '\n' ' ' > ${sdir[0]}/v2ports.txt && cat ${sdir[0]}/v2ports.txt`;
[[ -z $(echo "$v2rayports" | awk {'print $1'}) ]] && _v2rayports="null" || _v2rayports=$(echo "$v2rayports" | awk {'print $1'})
_tconex=$(netstat -nap | grep "$_v2rayports" | grep v2ray | grep ESTABLISHED |grep tcp6| awk {'print $5'} | awk -F ":" '{print $1}' | sort | uniq | wc -l)
#SPR & 
msg -bar3
msg -bar3
msg -tit
msg -ama "      PANNEL V2RAY Mod @drowkid01 v1.2 "
[[ ! -z $_v2rayports ]] && echo -e "       \e[97m\033[1;41mPUERTO ACTIVO :\033[0m \033[3;32m$_v2rayports\033[0m   \e[97m\033[1;41m ACTIVOS:\033[0m \033[3;32m\e[97m\033[1;41m $_tconex " ||  echo -e "  \e[97m\033[1;41mERROR A INICIAR V2RAY : \033[0m \033[3;32m FAIL\033[3;32m"
msg -bar3
bg=0
## INSTALADOR
[[ $(v2ray info |grep Group | wc -l) > 0 ]] || {
echo -e "\033[0;35m[\033[0;36m12\033[0;35m] \033[0;34m<\033[0;33m V2RAY BUGEADO \033[1;32m [ \033[0;34mFIX INSTALL \033[1;32m ]" 
bg=1
} 
[[ $bg = 0 ]] && {
echo -e "\033[0;35m[\033[0;36m1\033[0;35m] \033[0;34m<\033[0;33m  CAMBIAR PROTOCOLO  -> \033[1;32m [ \033[0;34m${_netW} \033[1;32m ]" 
echo -e "\033[0;35m[\033[0;36m2\033[0;35m] \033[0;34m<\033[0;33m  TLS ESTADO : -> ${_tlsN}"
echo -e "\033[0;35m[\033[0;36m3\033[0;35m] \033[0;34m<\033[0;33m  CAMBIAR PUERTO V2RAY \033[1;32m [ \033[0;32m$_v2rayports \033[1;32m]\n$(msg -bar3) "
## CONTROLER \033[0;31m [\033[0;32mON\033[0;31m] 
echo -e "\033[0;35m[\033[0;36m4\033[0;35m] \033[0;34m<\033[0;33m  AÑADIR USUARIO UUID "
#echo -e "\033[0;35m[\033[0;36mG\033[0;35m] \033[0;34m<\033[0;33m  AÑADIR USUARIO POR GRUPOS "
echo -e "\033[0;35m[\033[0;36m5\033[0;35m] \033[0;34m<\033[0;33m  ELIMINAR USUARIO UUID "
echo -e "\033[0;35m[\033[0;36m6\033[0;35m] \033[0;34m<\033[0;33m  RENOVAR USUARIO \033[1;32m ( ${_v2RegE} )"
echo -e "\033[0;35m[\033[0;36m7\033[0;35m] \033[0;34m<\033[0;33m  USUARIOS REGISTRADOS \033[1;32m ( ${_v2Reg} )"
echo -e "\033[0;35m[\033[0;36m8\033[0;35m] \033[0;34m<\033[0;33m  INFORMACION DE CUENTAS "
#echo -e "\033[0;35m[\033[0;36m9\033[0;35m] \033[0;34m<\033[0;33m  ESTADISTICAS DE CONSUMO "
echo -e "\033[0;35m[\033[0;36m9\033[0;35m] \033[0;34m<\033[0;33m  USUARIOS CONECTADOS "
#echo -ne "\033[1;32m [10] > " && msg -azu " LIMITADOR POR CONSUMO \033[1;33m( #BETA )"
echo -e "\033[0;35m[\033[0;36m10\033[0;35m] \033[0;34m<\033[0;33m  LIMPIADOR DE EXPIRADOS $statgen\n$(msg -bar3)"
echo -e "\033[0;35m[\033[0;36m13\033[0;35m] \033[0;34m<\033[0;33m  FUNCIONES BETAS \n$(msg -bar3)"
} || {
clear&&clear
	title "   V2RAY CON UN BUG - PARA FIXEAR"
	echo -e " \033[0;31m Selecciona, la opcion disponible"
	echo -e " y recuerda leer las indicaciones que sugiere\n"
	echo -e "  LUEGO DE ESTO, DEBERAS RECONFIGURAR TU METODO\n\033[0m"
	msg -bar3
echo -e "\033[0;35m[\033[0;36m12\033[0;35m] \033[0;34m<\033[0;33m V2RAY BUGEADO \033[1;32m [ \033[0;34mFIX INSTALL \033[1;32m ]" 
}
## DESISNTALAR
echo -ne "\033[1;32m [11] > " && msg -azu "\033[1;31mDESINSTALAR V2RAY"
echo -ne "$(msg -bar3)\n\033[1;32m [0] > " && msg -bra "\e[97m\033[1;41m VOLVER \033[1;37m"
msg -bar3
pid_inst () {
[[ $1 = "" ]] && echo -e "\033[1;31m[OFF]" && return 0
unset portas
portas_var=$(lsof -V -i -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep $1)
[[ ! -z ${portas_var} ]] && echo -e "\033[1;32m[ Servicio Activo ]" || echo -e "\033[1;31m[ Servicio Desactivado ]"
}
echo -e "         \e[97mEstado actual: $(pid_inst v2ray)"
unset selection
selection=$(selection_fun 13)
case ${selection} in
0)v2ray restart && exit ;;
1)protocolv2ray;;
2)tls;;
3)portv;;
4)addusr;;
5)_delUSR;;
6)echo -e " ANALIZANDO USUARIOS CADUCADOS"
renew;;
7)mosusr_kk;;
8)infocuenta;;
9)stats;;
#10)lim_port;;
10)limpiador_activador;;
11)unistallv2;;
12)_fixv2;;
13)_tools;;
esac
}
}

function v2ray_r(){
clear
config="/etc/v2ray/config.json"

#echo -ne "\033[1;32m[ INST \033[1;31m + \033[1;32mWORK ] "

_v2=`if netstat -tunlp | grep v2ray 1> /dev/null 2> /dev/null; then
[[ -e ${config} ]] && echo -e "\033[1;32m[ INST \033[1;31m+ \033[1;32mWORK ] " 
else
[[ -e ${config} ]] && echo -e "\033[1;32m[ INST \033[1;31m+ \033[1;33mLOADING \033[1;32m] " || echo -e "\033[1;32m[ \033[1;31mNO INST \033[1;32m] "
fi`;

configLOCK="/etc/v2ray/config.json.lock"
temp="/etc/v2ray/temp.json"
CGHlog='/var/log/v2ray/access.log'
v2rdir="/etc/v2r" && [[ ! -d $v2rdir ]] && mkdir $v2rdir
user_conf="/etc/v2r/user" && [[ ! -e $user_conf ]] && touch $user_conf
backdir="/etc/v2r/back" && [[ ! -d ${backdir} ]] && mkdir ${backdir}
tmpdir="$backdir/tmp"
[[ ! -e $v2rdir/conf ]] && echo "autBackup 0" > $v2rdir/conf
if [[ $(cat $v2rdir/conf | grep "autBackup") = "" ]]; then
	echo "autBackup 0" >> $v2rdir/conf
fi
barra="\033[0;31m=====================================================\033[0m"
numero='^[0-9]+$'
hora=$(printf '%(%H:%M:%S)T') 
fecha=$(printf '%(%D)T')

fun_bar () {
#==comando a ejecutar==
comando="$1"
#==interfas==
in=' ['
en=' ] '
full_in="??"
full_en='100%'
bar=("--------------------"
"=-------------------"
"]=------------------"
"[-]=-----------------"
"=[-]=----------------"
"-=[-]=---------------"
"--=[-]=--------------"
"---=[-]=-------------"
"----=[-]=------------"
"-----=[-]=-----------"
"------=[-]=----------"
"-------=[-]=---------"
"--------=[-]=--------"
"---------=[-]=-------"
"----------=[-]=------"
"-----------=[-]=-----"
"------------=[-]=----"
"-------------=[-]=---"
"--------------=[-]=--"
"---------------=[-]=-"
"----------------=[-]="
"-----------------=[-]"
"------------------=["
"-------------------="
"------------------=["
"-----------------=[-]"
"----------------=[-]="
"---------------=[-]=-"
"--------------=[-]=--"
"-------------=[-]=---"
"------------=[-]=----"
"-----------=[-]=-----"
"----------=[-]=------"
"---------=[-]=-------"
"--------=[-]=--------"
"-------=[-]=---------"
"------=[-]=----------"
"-----=[-]=-----------"
"----=[-]=------------"
"---=[-]=-------------"
"--=[-]=--------------"
"-=[-]=---------------"
"=[-]=----------------"
"[-]=-----------------"
"]=------------------"
"=-------------------"
"--------------------");
#==color==
in="\033[1;33m$in\033[0m"
en="\033[1;33m$en\033[0m"
full_in="\033[1;31m$full_in"
full_en="\033[1;32m$full_en\033[0m"

 _=$(
$comando > /dev/null 2>&1
) & > /dev/null
pid=$!
while [[ -d /proc/$pid ]]; do
	for i in "${bar[@]}"; do
		echo -ne "\r $in"
		echo -ne "ESPERE $en $in \033[1;31m$i"
		echo -ne " $en"
		sleep 0.2
	done
done
echo -e " $full_in $full_en"
sleep 0.2s
}



usrCONEC() {
[[ $log0 -le 1 ]] && {
#v2ray restart &> /dev/null
unset Users USERauth users UConc
v2ray clean &> /dev/null && let log0++ && clear 
}
msg -bar3
echo -e ""
echo -e " ESPERANDO A LA VERIFICACION DE IPS Y USUARIOS "
echo -e "      ESPERE UN MOMENTO PORFAVOR $log0"
echo -e ""
msg -bar3
fun_bar
msg -bar3
sleep 5s
clear&&clear
title2
msg -bar3
users="$(cat $config | jq -r .inbounds[].settings.clients[].email)"
IP_tconex=$(netstat -nap | grep "$v2rayports"| grep v2ray | grep ESTABLISHED | grep tcp6 | awk {'print $5'}| awk -F ":" '{print $1}' | sort | uniq)
#IP_tconex=$(netstat -nap | grep "$v2rayports"| grep v2ray | grep ESTABLISHED | awk {'print $5'} | awk -F ":" '{print $1}' | sort | uniq)
n=1
[[ -z $IP_tconex ]] && echo -e " NO HAY USUARIOS CONECTADOS!"
for i in $IP_tconex
do
	USERauth=$(cat ${CGHlog} | grep $i | grep accepted |awk '{print $7}'| sort | uniq)
	Users+="$USERauth\n"
done
echo -e " N) -|- USER -|- CONEXIONES "|column -t -s '-'
msg -bar3
for U in $users
	do
	CConT=$(echo -e "$Users" | grep $U |wc -l)
	[[ $CConT = 0 ]] && continue
	UConc+=" $n) -|- $U -|- $CConT\n"
	let n++
done
echo -e "$UConc"|column -t -s '-'
msg -bar3
continuar
read foo
}

install_ini () {
sudo apt-get install software-properties-common -y
add-apt-repository universe
apt update -y; apt upgrade -y
clear
msg -bar3
echo -e "\033[92m        -- INSTALANDO PAQUETES NECESARIOS -- "
msg -bar3
#bc
[[ $(dpkg --get-selections|grep -w "bc"|head -1) ]] || apt-get install bc -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "bc"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "bc"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install bc................... $ESTATUS "
#uuidgen
[[ $(dpkg --get-selections|grep -w "uuid-runtime"|head -1) ]] || sudo apt-get install uuid-runtime -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "uuid-runtime"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "uuid-runtime"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install uuid-runtime......... $ESTATUS "
#python
[[ $(dpkg --get-selections|grep -w "python"|head -1) ]] || apt-get install python -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "python"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "python"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install python............... $ESTATUS "
#pip
[[ $(dpkg --get-selections|grep -w "python-pip"|head -1) ]] || apt-get install python-pip -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "python-pip"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "python-pip"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install python-pip........... $ESTATUS "
#python3
[[ $(dpkg --get-selections|grep -w "python3"|head -1) ]] || apt-get install python3 -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "python3"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "python3"|head -1) ]] && ESTATUS=`echo -e "\e[3;32mINSTALADO\e[0m"` &>/dev/null
echo -e "\033[97m  # apt-get install python3.............. $ESTATUS "
#python3-pip
[[ $(dpkg --get-selections|grep -w "python3-pip"|head -1) ]] || apt-get install python3-pip -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "python3-pip"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "python3-pip"|head -1) ]] && ESTATUS=`echo -e "\e[3;32mINSTALADO\e[0m"` &>/dev/null
echo -e "\033[97m  # apt-get install python3-pip.......... $ESTATUS "
#QRENCODE
[[ $(dpkg --get-selections|grep -w "qrencode"|head -1) ]] || apt-get install qrencode -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "qrencode"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "qrencode"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install qrencode............. $ESTATUS "
#jq
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] || apt-get install jq -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "jq"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install jq................... $ESTATUS "
#curl
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] || apt-get install curl -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "curl"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install curl................. $ESTATUS "
#npm
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] || apt-get install npm -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "npm"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install npm.................. $ESTATUS "
#nodejs
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] || apt-get install nodejs -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "nodejs"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install nodejs............... $ESTATUS "
#socat
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] || apt-get install socat -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "socat"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install socat................ $ESTATUS "
#netcat
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] || apt-get install netcat -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install netcat............... $ESTATUS "
#netcat-traditional
[[ $(dpkg --get-selections|grep -w "netcat-traditional"|head -1) ]] || apt-get install netcat-traditional -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat-traditional"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "netcat-traditional"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install netcat-traditional... $ESTATUS "
#net-tools
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] || apt-get net-tools -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "net-tools"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install net-tools............ $ESTATUS "
#cowsay
[[ $(dpkg --get-selections|grep -w "cowsay"|head -1) ]] || apt-get install cowsay -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "cowsay"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "cowsay"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install cowsay............... $ESTATUS "
#figlet
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] || apt-get install figlet -y &>/dev/null
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "figlet"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install figlet............... $ESTATUS "
#lolcat
apt-get install lolcat -y &>/dev/null
sudo gem install lolcat &>/dev/null
[[ $(dpkg --get-selections|grep -w "lolcat"|head -1) ]] || ESTATUS=`echo -e "\033[91mFALLO DE INSTALACION"` &>/dev/null
[[ $(dpkg --get-selections|grep -w "lolcat"|head -1) ]] && ESTATUS=`echo -e "\033[92mINSTALADO"` &>/dev/null
echo -e "\033[97m  # apt-get install lolcat............... $ESTATUS "

msg -bar3
echo -e "\033[92m La instalacion de paquetes necesarios a finalizado"
msg -bar3
echo -e "\033[97m Si la instalacion de paquetes tiene fallas"
echo -ne "\033[97m Puede intentar de nuevo [s/n]: "
read inst
[[ $inst = @(s|S|y|Y) ]] && install_ini
}


autB(){
	if [[ ! $(cat $v2rdir/conf | grep "autBackup" | cut -d " " -f2) = "0" ]]; then
		autBackup
	fi
 }
 
restore(){
	clear

	unset num
	unset opcion
	unset _res

	if [[ -z $(ls $backdir) ]]; then
		title "	no se encontraron respaldos"
		sleep 0.5
		return
	fi

	num=1
	title "	   Lista de Respaldos creados"
	blanco "	      nom  \033[0;31m| \033[1;37mfechas \033[0;31m|  \033[1;37mhora"
	msg -bar3
	for i in $(ls $backdir); do
		col "$num)" "$i"
		_res[$num]=$i
		let num++
	done
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco " cual desea restaurar?" 0
	read opcion

	[[ $opcion = 0 ]] && return
	[[ -z $opcion ]] && blanco "\n deves seleccionar una opcion!" && sleep 0.1 && return
	[[ ! $opcion =~ $numero ]] && blanco "\n solo deves ingresar numeros!" && sleep 0.1 && return
	[[ $opcion -gt ${#_res[@]} ]] && blanco "\n solo numeros entre 0 y ${#_res[@]}" && sleep 0.1 && return

	mkdir $backdir/tmp
	tar xpf $backdir/${_res[$opcion]} -C $backdir/tmp/

	clear
	title "	Archivos que se restauran"

	if rm -rf $config && cp $tmpdir/config.json $temp; then
		sleep 0.1
		echo "cat $temp | jq '.inbounds[].streamSettings.tlsSettings += {certificates:[{certificateFile:\"/data/v2ray.crt\",keyFile:\"/data/v2ray.key\"}]}' >> $config" | bash
		chmod 777 $config
		rm $temp
		blanco " /etc/v2ray/config.json..." && verde "[ok]" 
	else
		blanco " /etc/v2ray/config.json..." && rojo "[fail]"	
	fi

	if rm -rf $user_conf && cp $tmpdir/user $user_conf; then
		blanco " /etc/v2r/user..." && verde "[ok]"
	else
		blanco " /etc/v2r/user..." && rojo "[fail]"
	fi
    [[ -e $tmpdir/fullchain.cer ]] && mv $tmpdir/fullchain.cer $tmpdir/fullchain.crt
	if rm -rf /data && mkdir /data && cp $tmpdir/*.crt /data/v2ray.crt && cp $tmpdir/*.key /data/v2ray.key; then
		blanco " /data/v2ray.crt..." && verde "[ok]"
		blanco " /data/v2ray.key..." && verde "[ok]"
	else
		blanco " /data/v2ray.crt..." && rojo "[fail]"
		blanco " /data/v2ray.key..." && rojo "[fail]"
		msg -bar3
		echo -e "VALIDA TU CERTIFICADO SSL "
		v2ray tls 
	fi
	rm -rf $tmpdir
	msg -bar3
	continuar
	read foo
}

server(){
	clear

	if [[ $(npm ls -g | grep "http-server") = "" ]]; then
		npm install --global http-server
		clear
	fi

	if [[ $(ps x | grep "http-server" | grep -v grep) = "" ]]; then
		screen -dmS online http-server /etc/v2r/back/ --port 95 -s
		title "	Respaldos en linea"
		col "su url:" "http://$(wget -qO- ipv4.icanhazip.com):95"
		msg -bar3
		continuar
		read foo
	else
		killall http-server
		title "	servidor detenido..."
		sleep 0.1
	fi
 }
 
autBackup(){
	unset fecha
	unset hora
	unset tmp
	unset back
	unset cer
	unset key
	#fecha=`date +%d-%m-%y-%R`
	fecha=`date +%d-%m-%y`
	hora=`date +%R`
	tmp="$backdir/tmp" && [[ ! -d ${tmp} ]] && mkdir ${tmp}
	back="$backdir/v2r___${fecha}___${hora}.tar"
	cer=$(cat /etc/v2ray/config.json | jq -r ".inbounds[].streamSettings.tlsSettings.certificates[].certificateFile")
	key=$(cat /etc/v2ray/config.json | jq -r ".inbounds[].streamSettings.tlsSettings.certificates[].keyFile")

	cp $user_conf $tmp
	cp $config $tmp
	[[ ! $cer = null ]] && [[ -e $cer ]] && cp $cer $tmp
	[[ ! $key = null ]] && [[ -e $cer ]] && cp $key $tmp

	cd $tmp
	tar -cpf $back *
	cp $back /var/www/html/v2rayBack.tar && echo -e "
 Descargarlo desde cualquier sitio con acceso WEB
  LINK : http://$(wget -qO- ifconfig.me):81/v2rayBack.tar \033[0m
-------------------------------------------------------"
read -p "ENTER PARA CONTINUAR"
	rm -rf $tmp
 }
 
on_off_res(){
	if [[ $(cat $v2rdir/conf | grep "autBackup" | cut -d " " -f2) = "0" ]]; then
		echo -e "\033[0;31m[off]"
	else
		echo -e "\033[1;92m[on]"
	fi
 }

blanco(){
	[[ !  $2 = 0 ]] && {
		echo -e "\033[1;37m$1\033[0m"
	} || {
		echo -ne " \033[1;37m$1:\033[0m "
	}
}

verde(){
	[[ !  $2 = 0 ]] && {
		echo -e "\033[1;32m$1\033[0m"
	} || {
		echo -ne " \033[1;32m$1:\033[0m "
	}
}

rojo(){
	[[ !  $2 = 0 ]] && {
		echo -e "\033[1;31m$1\033[0m"
	} || {
		echo -ne " \033[1;31m$1:\033[0m "
	}
}

col(){

	nom=$(printf '%-55s' "\033[0;92m${1} \033[0;31m>> \033[1;37m${2}")
	echo -e "	$nom\033[0;31m${3}   \033[0;92m${4}\033[0m"
}

col2(){

	echo -e " \033[1;91m$1\033[0m \033[1;37m$2\033[0m"
}

vacio(){

	blanco "\n no se puede ingresar campos vacios..."
}

cancelar(){

	echo -e "\n \033[3;49;31minstalacion cancelada...\033[0m"
}

continuar(){

	echo -e " \033[3;49;32mEnter para continuar...\033[0m"
}

title2(){
v2rayports=`lsof -V -i tcp -P -n | grep -v "ESTABLISHED" |grep -v "COMMAND" | grep "LISTEN" | grep v2ray | awk '{print substr($9,3); }' > /tmp/v2ray.txt && echo | cat /tmp/v2ray.txt | tr '\n' ' ' > ${sdir[0]}/v2rayports.txt && cat ${sdir[0]}/v2rayports.txt` > /dev/null 2>&1 
v2rayports=$(echo $v2rayports | awk {'print $1'})
_tconex=$(netstat -nap | grep "$v2rayports" | grep v2ray | grep ESTABLISHED| grep tcp6 | awk {'print $5'} | awk -F ":" '{print $1}' | sort | uniq | wc -l)
	v1=$(cat ${sdir[0]}/v-local.log)
	v2=$(cat /bin/ejecutar/v-new.log)
	msg -bar3
	[[ $v1 = $v2 ]] && echo -e "   \e[97m\033[1;44m MENU V2RAY LITE [$v1] POWER BY @drowkid01 \033[0m" || echo -e " \e[97m\033[1;44m MENU V2RAY LITE POWER BY @drowkid01 [$v1] >> \033[1;92m[$v2] \033[0m"
[[ ! -z $v2rayports ]] && echo -e "       \e[97m\033[1;44mPUERTO ACTIVO :\033[0m \033[3;32m$v2rayports\033[0m   \e[97m\033[1;44m ACTIVOS:\033[0m \033[3;32m\e[97m\033[1;41m $_tconex " ||  echo -e "  \e[97m\033[1;41mERROR A INICIAR V2RAY : \033[0m \033[3;32m FAIL\033[3;32m"
	}

title(){
	msg -bar3
	blanco "$1"
	msg -bar3
}

userDat(){
	blanco "	N°    Usuarios 		  fech exp   dias"
	msg -bar3
}

#============================================
domain_check() {
	ssl_install_fun
    clear
    msg -bar3
    echo -e "   \033[1;49;37mgenerador de certificado ssl/tls\033[0m"
    msg -bar3
    echo -e " \033[1;49;37mingrese su dominio (ej: midominio.com.ar)\033[0m"
    echo -ne ' \033[3;49;31m>>>\033[0m '
    read domain

    echo -e "\n \033[1;49;36mOteniendo resolucion dns de su dominio...\033[0m"
    domain_ip=$(ping "${domain}" -c 1 | sed '1{s/[^(]*(//;s/).*//;q}')

    echo -e "\n \033[1;49;36mOteniendo IP local...\033[0m"
    local_ip=$(wget -qO- ipv4.icanhazip.com)
    sleep 0.5

    while :
    do
    if [[ $(echo "${local_ip}" | tr '.' '+' | bc) -eq $(echo "${domain_ip}" | tr '.' '+' | bc) ]]; then
            clear
            msg -bar3
            echo -e " \033[1;49;37mSu dominio: ${domain}\033[0m"
            msg -bar3
            echo -e " \033[1;49;37mIP dominio:\033[0m  \033[1;49;32m${domain_ip}\033[0m"
            echo -e " \033[1;49;37mIP local:\033[0m    \033[1;49;32m${local_ip}\033[0m"
            msg -bar3
            echo -e "      \033[1;49;32mComprovacion exitosa\033[0m"
            echo -e " \033[1;49;37mLa IP de su dominio coincide\n con la IP local, desea continuar?\033[0m"
            msg -bar3
            echo -ne " \033[1;49;37msi o no [S/N]:\033[0m "
            read opcion
            case $opcion in
                [Yy]|[Ss]) port_exist_check;;
                [Nn]) cancelar && sleep 0.5;;
                *) echo -e "\n \033[1;49;37mselecione (S) para si o (N) para no!\033[0m" && sleep 0.5 && continue;;
            esac
    else
            clear
            msg -bar3
            echo -e " \033[1;49;37mSu dominio: ${domain}\033[0m"
            msg -bar3
            echo -e " \033[1;49;37mIP dominio:\033[0m  \033[3;49;31m${domain_ip}\033[0m"
            echo -e " \033[1;49;37mIP local:\033[0m    \033[3;49;31m${local_ip}\033[0m"
            msg -bar3
            echo -e "      \033[3;49;31mComprovacion fallida\033[0m"
            echo -e " \033[4;49;97mLa IP de su dominio no coincide\033[0m\n         \033[4;49;97mcon la IP local\033[0m"
            msg -bar3
            echo -e " \033[1;49;36m> Asegúrese que se agrego el registro"
            echo -e "   (A) correcto al nombre de dominio."
            echo -e " > Asegurece que su registro (A)"
            echo -e "   no posea algun tipo de seguridad"
            echo -e "   adiccional y que solo resuelva DNS."
            echo -e " > De lo contrario, V2ray no se puede"
            echo -e "   utilizar normalmente...\033[0m"
            msg -bar3
            echo -e " \033[1;49;37mdesea continuar?"
            echo -ne " si o no [S/N]:\033[0m "
            read opcion
            case $opcion in
                [Yy]|[Ss]) port_exist_check;;
                [Nn]) cancelar && sleep 0.5;;
                *) echo -e "\n \033[1;49;37mselecione (S) para si o (N) para no!\033[0m" && sleep 0.2 && continue;;
            esac
        fi
        break
    done
}

port_exist_check() {
    while :
    do
    clear
    msg -bar3
    echo -e " \033[1;49;37mPara la compilacion del certificado"
    echo -e " se requiere que los siguientes puerto"
    echo -e " esten libres."
    echo -e "        '80' '443'"
    echo -e " este script intentara detener"
    echo -e " cualquier proseso que este"
    echo -e " usando estos puertos\033[0m"
    msg -bar3
    echo -e " \033[1;49;37mdesea continuar?"
    echo -ne " [S/N]:\033[0m "
    read opcion

    case $opcion in
        [Ss]|[Yy])         
                    ports=('80' '443')
                    clear
                        msg -bar3
                        echo -e "      \033[1;49;37mcomprovando puertos...\033[0m"
                        msg -bar3
                        sleep 0.2
                        for i in ${ports[@]}; do
                            [[ 0 -eq $(lsof -i:$i | grep -i -c "listen") ]] && {
                                echo -e "    \033[3;49;32m$i [OK]\033[0m" 
                            } || {
                                echo -e "    \033[3;49;31m$i [fail]\033[0m"
                            }
                        done
                        msg -bar3
                        for i in ${ports[@]}; do
                            [[ 0 -ne $(lsof -i:$i | grep -i -c "listen") ]] && {
                                echo -ne "       \033[1;49;37mliberando puerto $i...\033[1;49;37m "
                                lsof -i:$i | awk '{print $2}' | grep -v "PID" | xargs kill -9
                                echo -e "\033[1;49;32m[OK]\033[0m"
                            }
                        done
                        ;;
        [Nn]) cancelar && sleep 0.2 && break;;
        *) echo -e "\n \033[1;49;37mselecione (S) para si o (N) para no!\033[0m" && sleep 0.2;;
    esac
    continuar
    read foo
    ssl_install
    break
    done
}

ssl_install() {
    while :
    do

    if [[ -f "/data/v2ray.key" || -f "/data/v2ray.crt" ]]; then
        clear
        msg -bar3
        echo -e " \033[1;49;37mya existen archivos de certificados"
        echo -e " en el directorio asignado.\033[0m"
        msg -bar3
        echo -e " \033[1;49;37mENTER para canselar la instacion."
        echo -e " 'S' para eliminar y continuar\033[0m"
        msg -bar3
        echo -ne " opcion: "
        read ssl_delete
        case $ssl_delete in
        [Ss]|[Yy])
                    rm -rf /data/*
                    echo -e " \033[3;49;32marchivos removidos..!\033[0m"
                    sleep 0.2
                    ;;
        *) cancelar && sleep 0.2 && break;;
        esac
    fi

    if [[ -f "$HOME/.acme.sh/${domain}_ecc/${domain}.key" || -f "$HOME/.acme.sh/${domain}_ecc/${domain}.cer" ]]; then
        msg -bar3
        echo -e " \033[1;49;37mya existe un almacer de certificado"
        echo -e " bajo este nombre de dominio\033[0m"
        msg -bar3
        echo -e " \033[1;49;37m'ENTER' cansela la instalacion"
        echo -e " 'D' para eliminar y continuar"
        echo -e " 'R' para restaurar el almacen crt\033[0m"
        msg -bar3
        echo -ne " opcion: "
        read opcion
        case $opcion in
            [Dd])
                        echo -e " \033[1;49;92meliminando almacen cert...\033[0m"
                        sleep 0.2
                        rm -rf $HOME/.acme.sh/${domain}_ecc
                        ;;
            [Rr])
                        echo -e " \033[1;49;92mrestaurando certificados...\033[0m"
                        sleep 0.2
                        "$HOME"/.acme.sh/acme.sh --installcert -d "${domain}" --fullchainpath /data/v2ray.crt --keypath /data/v2ray.key --ecc
			            echo "cat $temp | jq '.inbounds[].streamSettings.tlsSettings += {certificates:[{certificateFile:\"/data/v2ray.crt\",keyFile:\"/data/v2ray.key\"}]}' | jq '.inbounds[] += {domain:\"$domi\"}' | jq '.inbounds[].streamSettings += {security:\"tls\"}' >> $config" | bash
			            restart_v2r
                        echo -e " \033[1;49;37mrestauracion completa...\033[0m\033[1;49;92m[ok]\033[0m"
                        break
                        ;;
            *) cancelar && sleep 0.2 && break;;
        esac
    fi
    acme
    break
    done 
}

ssl_install_fun() {
    apt install socat netcat -y
    curl https://get.acme.sh | sh
}

acme() {
    clear
    msg -bar3
    echo -e " \033[1;49;37mcreando nuevos certificado ssl/tls\033[0m"
	#msg -bar3
#	read -p " Ingrese correo Para Validar el acme SSL : " corrio
	msg -bar3
	wget -O -  https://get.acme.sh | sh -s email=$corrio
    msg -bar3
    if "$HOME"/.acme.sh/acme.sh --issue -d "${domain}" --standalone -k ec-256 --force --test; then
        echo -e "\n           \033[1;49;37mSSL La prueba del certificado\n se emite con éxito y comienza la emisión oficial\033[0m\n"
        rm -rf "$HOME/.acme.sh/${domain}_ecc"
        sleep 0.2
    else
        echo -e "\n \033[4;49;31mError en la emisión de la prueba del certificado SSL\033[0m"
        msg -bar3
        rm -rf "$HOME/.acme.sh/${domain}_ecc"
        stop=1
    fi

    if [[ 0 -eq $stop ]]; then

    if "$HOME"/.acme.sh/acme.sh --issue -d "${domain}" --standalone -k ec-256 --force; then
        echo -e "\n \033[1;49;37mSSL El certificado se genero con éxito\033[0m"
        msg -bar3
        sleep 0.2
        [[ ! -d /data ]] && mkdir /data
        if "$HOME"/.acme.sh/acme.sh --installcert -d "${domain}" --fullchainpath /data/v2ray.crt --keypath /data/v2ray.key --ecc --force; then
            msg -bar3
            mv $config $temp
            echo "cat $temp | jq '.inbounds[].streamSettings.tlsSettings += {certificates:[{certificateFile:\"/data/v2ray.crt\",keyFile:\"/data/v2ray.key\"}]}' | jq '.inbounds[] += {domain:\"$domain\"}' | jq '.inbounds[].streamSettings += {security:\"tls\"}' >> $config" | bash
            chmod 777 $config
            rm $temp
            restart_v2r
            echo -e "\n \033[1;49;37mLa configuración del certificado es exitosa\033[0m"
            msg -bar3
            echo -e "      /data/v2ray.crt"
            echo -e "      /data/v2ray.key"
            msg -bar3
            sleep 0.2
        fi
    else
        echo -e "\n \033[4;49;31mError al generar el certificado SSL\033[0m"
        msg -bar3
        rm -rf "$HOME/.acme.sh/${domain}_ecc"
    fi
    fi
    continuar
    read foo
}

#============================================

restart_v2r(){
	v2ray restart
	#echo "reiniciando"
}

add_user(){
	unset seg opcion
	seg=$(date +%s)
	while :
	do
	clear
	users="$(cat $config | jq -r .inbounds[].settings.clients[].email)"
	title "		CREAR USUARIO V2RAY"
	userDat
	n=0
	for i in $users
	do
		unset DateExp
		unset seg_exp
		unset exp
		[[ $i = null ]] && {
			i="default"
			a='*'
			DateExp=" unlimit"
			col "$a)" "$i" "$DateExp"
		} || {
			DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
			seg_exp=$(date +%s --date="$DateExp")
			exp="[$(($(($seg_exp - $seg)) / 86400))]"

			col "$n)" "$i" "$DateExp" "$exp"
		}
		let n++
	done
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco "NOMBRE DEL NUEVO USUARIO" 0
	read opcion
	[[ -z $opcion ]] && vacio && sleep 0.3 && continue
	[[ $opcion = 0 ]] && break
	[[ ! -z "$(cat < $user_conf | grep -w "${opcion}")" ]] && echo -e " NOMBRE ${opcion} YA ESTA REGISTRADO " && sleep 1.1 && continue
	blanco "DURACION EN DIAS" 0
	read dias
	espacios=$(echo "$opcion" | tr -d '[[:space:]]')
	opcion=$espacios
	mv $config $temp
	num=$(jq '.inbounds[].settings.clients | length' $temp)
	new=".inbounds[].settings.clients[$num]"
	new_id=$(uuidgen)
	new_mail="email:\"$opcion\""
	aid=$(jq '.inbounds[].settings.clients[0].alterId' $temp)
	echo jq \'$new += \{alterId:${aid},id:\"$new_id\","$new_mail"\}\' $temp \> $config | bash
	echo "$opcion | $new_id | $(date '+%y-%m-%d' -d " +$dias days")" >> $user_conf
	chmod 777 $config
	rm $temp
	clear
	msg -bar3
	blanco "	Usuario $opcion creado Exitosamente"
	msg -bar3
	restart_v2r
	sleep 0.2
	#fun_bar
	#-------------------------------------------------------------
		ps=${opcion} #$(jq .inbounds[].settings.clients[$opcion].email $config) && [[ $ps = null ]] && ps="default"
		id=${new_id} #$(jq .inbounds[].settings.clients[$opcion].id $config)
		aid=$(jq .inbounds[].settings.clients[].alterId $config | head -1)
		add=$(jq '.inbounds[].domain' $config) && [[ $add = null ]] && add=$(wget -qO- ipv4.icanhazip.com)
		host=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $host = null ]] && host='aqui.tu.host'
		net=$(jq '.inbounds[].streamSettings.network' $config)
		[[ $net = '"grpc"' ]] && path=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || path=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
		port=$(jq '.inbounds[].port' $config)
		tls=$(jq '.inbounds[].streamSettings.security' $config)
		addip=$(wget -qO- ifconfig.me)
		clear
		msg -bar3
		blanco " Usuario: $ps"
		msg -bar3
		col2 "Remarks:" "$ps"
		col2 "Domain:" "$add" 
		col2 "IP-Address:" "$addip"
		col2 "Port:" "$port"
		col2 "id:" "$id"
		col2 "alterId:" "$aid"
		col2 "network:" "$net"
		[[ $tls = '"tls"' ]] && col2 "TLS:" "ABIERTO" || col2 "TLS:" " CERRADO"
		[[ $net = '"grpc"' ]] && col2 "Mode:" " GUN" || col2 "Head Type:" "none"
		col2 "security:" "none"
		[[ ! $host = '' ]] && col2 "Host/SNI:" "$host"
		[[ $net = '"grpc"' ]] && col2 "ServiceName:" "$path" || col2 "Path:" "$path"
		msg -bar3
		blanco "              VMESS LINK CONFIG"
		msg -bar3
		vmess
		msg -bar3
		echo -e "  ESTA CONFIG SOLO SE MUESTRA UNA VEZ AQUI \n SI QUIERES VOLVER A VERLA VE A LA OPCION 4 \n  Y BALLASE A LA BERGA PERRO :V"
		msg -bar3
		continuar
		read foo
	#---------------------------------------------------------------------
    done
}

renew(){
	while :
	do
		unset user
		clear
		title "		RENOVAR USUARIOS"
		userDat
		userEpx=$(cut -d " " -f1 $user_conf)
		n=1
		for i in $userEpx
		do
			DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
			seg_exp=$(date +%s --date="$DateExp")
			[[ "$seg" -gt "$seg_exp" ]] && {
				col "$n)" "$i" "$DateExp" "\033[0;31m[Exp]"
				uid[$n]="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f2|tr -d '[[:space:]]')"
				user[$n]=$i
				let n++
			}
		done
		[[ -z ${user[1]} ]] && blanco "		No hay expirados"
		msg -bar3
		col "0)" "VOLVER"
		msg -bar3
		blanco "NUMERO DE USUARIO A RENOVAR" 0
		read opcion

		[[ -z $opcion ]] && vacio && sleep 0.3 && continue
		[[ $opcion = 0 ]] && break

		[[ ! $opcion =~ $numero ]] && {
			blanco " solo numeros apartir de 1"
			sleep 0.2
		} || {
			[[ $opcion>=${n} ]] && {
				let n--
				blanco "solo numero entre 1 y $n"
				sleep 0.2
		} || {
			blanco "DURACION EN DIAS" 0
			read dias

			mv $config $temp
			num=$(jq '.inbounds[].settings.clients | length' $temp)
			aid=$(jq '.inbounds[].settings.clients[0].alterId' $temp)
			echo "cat $temp | jq '.inbounds[].settings.clients[$num] += {alterId:${aid},id:\"${uid[$opcion]}\",email:\"${user[$opcion]}\"}' >> $config" | bash
			sed -i "/${user[$opcion]}/d" $user_conf
			echo "${user[$opcion]} | ${uid[$opcion]} | $(date '+%y-%m-%d' -d " +$dias days")" >> $user_conf
			chmod 777 $config
			rm $temp
			clear
			msg -bar3
			blanco "	Usuario ${user[$opcion]} renovado Exitosamente"
			msg -bar3
			restart_v2r
			sleep 0.2
		  }
		}
	done
}

autoDel(){
	seg=$(date +%s)
	while :
	do
		unset users
		users=$(cat $config | jq .inbounds[].settings.clients[] | jq -r .email)
		n=0
		for i in $users
		do
			[[ ! $i = null ]] && {
				DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
				seg_exp=$(date +%s --date="$DateExp")
				[[ "$seg" -gt "$seg_exp" ]] && {
					mv $config $temp
					echo jq \'del\(.inbounds[].settings.clients[$n]\)\' $temp \> $config | bash
					chmod 777 $config
					rm $temp
					continue
				}
			}
			let n++
			done
			break
		done
		restart_v2r
	}

dell_user(){
	unset seg
	seg=$(date +%s)
	while :
	do
	clear
	users=$(cat $config | jq .inbounds[].settings.clients[] | jq -r .email)
	title "	ELIMINAR USUARIO V2RAY"
	userDat
	n=0
	for i in $users
	do
		userd[$n]=$i
		unset DateExp
		unset seg_exp
		unset exp

		[[ $i = null ]] && {
			i="default"
			a='*'
			DateExp=" unlimit"
			col "$a)" "$i" "$DateExp"
		} || {
			DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
			seg_exp=$(date +%s --date="$DateExp")
			exp="[$(($(($seg_exp - $seg)) / 86400))]"
			col "$n)" "$i" "$DateExp" "$exp"
		}
		p=$n
		let n++
	done
	userEpx=$(cut -d " " -f 1 $user_conf)
	for i in $userEpx
	do	
		DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
		seg_exp=$(date +%s --date="$DateExp")
		[[ "$seg" -gt "$seg_exp" ]] && {
			col "$n)" "$i" "$DateExp" "\033[0;31m[Exp]"
			expUser[$n]=$i
		}
		let n++
	done
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco "NUMERO DE USUARIO A ELIMINAR" 0
	read opcion

	[[ -z $opcion ]] && vacio && sleep 0.3 && continue
	[[ $opcion = 0 ]] && break

	[[ ! $opcion =~ $numero ]] && {
		blanco " solo numeros apartir de 1"
		sleep 0.2
	} || {
		let n--
		[[ $opcion -gt ${n} ]] && {
			blanco "solo numero entre 1 y $n"
			sleep 0.2
		} || {
			[[ $opcion>=${p} ]] && {
				sed -i "/${expUser[$opcion]}/d" $user_conf
				_us="${expUser[$opcion]}"
			} || {
			sed -i "/${userd[$opcion]}/d" $user_conf
			_us="${userd[$opcion]}"
			mv $config $temp
			echo jq \'del\(.inbounds[].settings.clients[$opcion]\)\' $temp \> $config | bash
			chmod 777 $config
			rm $temp
			clear
			msg -bar3
			#blanco "	Usuario $(jq .inbounds[].settings.clients[$opcion].email $config) eliminado"
			blanco "	USUARIO ${_us} ELIMINADO EXITOSAMENTE !!"
			msg -bar3
			restart_v2r
			}
			sleep 0.2
		}
	}
	done
}

fun_lock(){
	unset seg
	local seg=$(date +%s)
	while :
	do
	clear
	users=$(cat $config | jq .inbounds[].settings.clients[] | jq -r .email)
	title " BLOQUEAR USUARIO Y MANDARLO AL LIMBOOOOO"
	userDat
	local n=1
	for i in $users
	do
		unset DateExp
		unset seg_exp
		unset exp
		[[ $i = null ]] || {
			DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
			seg_exp=$(date +%s --date="$DateExp")
			exp="[$(($(($seg_exp - $seg)) / 86400))]"
			col "$n)" "$i" "$DateExp" "$exp"
			local uid[$n]="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f2|tr -d '[[:space:]]')"
			local user[$n]=$i
			local p=$n
			let n++
		}
	done
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco "NUMERO DE USUARIO A BLOQUEAR" 0
	read opcion

	[[ -z $opcion ]] && vacio && sleep 0.3 && continue
	[[ $opcion = 0 ]] && break

	[[ ! $opcion =~ $numero ]] && {
		blanco " solo numeros apartir de 1"
		sleep 0.2
	} || {
		let n--
		[[ $opcion -gt ${n} ]] && {
			blanco "solo numero entre 1 y $n"
			sleep 0.2
		} || {
		#echo -e " OPCION ${opcion} / USER : ${user[$opcion]}"
		local LOCKDATA="$(cat ${user_conf}|grep -w "${user[$opcion]}"|cut -d'|' -f3)"
		local tempo=$(date +%s --date="$LOCKDATA")
		local dias="$(($(($tempo - $seg)) / 86400))"
			[[ $opcion>=${p} ]] && {
			echo "${user[$opcion]} | ${uid[$opcion]} | $(date '+%y-%m-%d' -d " +$dias days") |${dias}" >> $configLOCK
			#echo "${user[$opcion]} | ${uid[$opcion]} | $(date '+%y-%m-%d' -d " +$dias days") |${dias}" 
			mv $config $temp
			echo jq \'del\(.inbounds[].settings.clients[$opcion]\)\' $temp \> $config | bash
			chmod 777 $config
			sed -i "/${user[$opcion]}/d" $user_conf
			rm $temp
			chmod 777 $configLOCK
			#read -p "PAUSE"
			} || {
			echo "${user[$opcion]} | ${uid[$opcion]} | $(date '+%y-%m-%d' -d " +$dias days") |${dias}" >> $configLOCK
			#echo "${user[$opcion]} | ${uid[$opcion]} | $(date '+%y-%m-%d' -d " +$dias days") |${dias}"
			mv $config $temp
			echo jq \'del\(.inbounds[].settings.clients[$opcion]\)\' $temp \> $config | bash
			chmod 777 $config
			rm $temp
			sed -i "/${user[$opcion]}/d" $user_conf
			chmod 777 $configLOCK
			#read -p "PAUSE"
			clear
			msg -bar3
			#blanco "	Usuario $(jq .inbounds[].settings.clients[$opcion].email $config) eliminado"
			blanco " USUARIO ${user[$opcion]} NUM: ${opcion} ENVIADO AL LIMBOOOO !!"
			msg -bar3
			restart_v2r
			}
			sleep 0.2
		}
	}
done
}

fun_unlock(){
#echo "${user[$opcion]} | ${uid[$opcion]} | $(date '+%y-%m-%d' -d " +$dias days")" >> $configLOCK
	while :
	do
		unset user n
		clear
		title " DESBOQUEAR USUARIOS DEL LIMBOOOOO!!!"
		userDat
		userEpx=$(cut -d " " -f1 $configLOCK)
		n=1
		for i in $userEpx
		do
			DateExp="$(cat ${configLOCK}|grep -w "${i}"|cut -d'|' -f4)"
			#seg_exp=$(date +%s --date="$DateExp")
			#[[ "$seg" -gt "$seg_exp" ]] && {
				col "$n)" "$i" "$DateExp DIAS" "\033[0;31m[LOCK]"
				#col "$n)" "$i" "\033[0;31m[LOCK]"
				local uid[$n]="$(cat ${configLOCK}|grep -w "${i}"|cut -d'|' -f2|tr -d '[[:space:]]')"
				local user[$n]=$i
				local tiempito[$n]="$(cat ${configLOCK}|grep -w "${i}"|cut -d'|' -f4|tr -d '[[:space:]]')"
				let n++
			#}
		done
		[[ -z ${user[1]} ]] && blanco "		No hay bloqueados!!!"
		msg -bar3
		col "0)" "VOLVER"
		msg -bar3
		blanco "NUMERO DE USUARIO A DESBLOQUEAR" 0
		read opcion

		[[ -z $opcion ]] && vacio && sleep 0.3 && continue
		[[ $opcion = 0 ]] && break

		[[ ! $opcion =~ $numero ]] && {
			blanco " solo numeros apartir de 1"
			sleep 0.2
		} || {
			[[ $opcion>=${n} ]] && {
				let n--
				blanco "solo numero entre 1 y $n"
				sleep 0.2
		} || {
			#blanco "DURACION EN DIAS" 0
			local dias=${tiempito[$opcion]}
			mv $config $temp
			num=$(jq '.inbounds[].settings.clients | length' $temp)
			aid=$(jq '.inbounds[].settings.clients[0].alterId' $temp)
			echo "cat $temp | jq '.inbounds[].settings.clients[$num] += {alterId:${aid},id:\"${uid[$opcion]}\",email:\"${user[$opcion]}\"}' >> $config" | bash
			sed -i "/${user[$opcion]}/d" ${configLOCK}
			echo "${user[$opcion]} | ${uid[$opcion]} | $(date '+%y-%m-%d' -d " +$dias days")" >> $user_conf
			chmod 777 $config
			rm $temp
			clear
			msg -bar3
			blanco " USUARIO ${user[$opcion]} RETIRADO DEL LIMBOOOOOOO!!"
			msg -bar3
			restart_v2r
			sleep 0.2
		  }
		}
	done
}

_lo_un(){
clear
msg -bar3
echo -e "\033[0;35m [${cor[2]}1\033[0;35m]\033[0;33m ${flech}\033[0;33m [!] BLOQUEAR USUARIO V2RAY "
echo -e "\033[0;35m [${cor[2]}2\033[0;35m]\033[0;33m ${flech}\033[0;33m [!] DESBLOQUEAR USUARIO V2RAY "
msg -bar3
echo -e " \033[0;35m[${cor[2]}0\033[0;35m]\033[0;33m ${flech} $(msg -bra "\033[1;43m[ Salir ]\e[0m")"
msg -bar3 
	selection=$(selection_fun 2)
	case ${selection} in
		1)
			fun_lock
		;;
		2)
			fun_unlock
		;;
		esac
}

view_user(){
	unset seg
	seg=$(date +%s)
	while :
	do

		clear
		users=$(cat $config | jq .inbounds[].settings.clients[] | jq -r .email)

		title "	VER USUARIO V2RAY"
		userDat

		n=1
		for i in $users
		do
			unset DateExp
			unset seg_exp
			unset exp

			[[ $i = null ]] && {
				i="Admin"
				DateExp=" Ilimitado"
			} || {
				DateExp="$(cat ${user_conf}|grep -w "${i}"|cut -d'|' -f3)"
				seg_exp=$(date +%s --date="$DateExp")
				exp="[$(($(($seg_exp - $seg)) / 86400))]"
			}

			col "$n)" "$i" "$DateExp" "$exp"
			let n++
		done

		msg -bar3
		col "0)" "VOLVER"
		msg -bar3
		blanco "VER DATOS DEL USUARIO" 0
		read opcion

		[[ -z $opcion ]] && vacio && sleep 0.3 && continue
		[[ $opcion = 0 ]] && break

		let opcion--

		ps=$(jq .inbounds[].settings.clients[$opcion].email $config) && [[ $ps = null ]] && ps="default"
		id=$(jq .inbounds[].settings.clients[$opcion].id $config)
		aid=$(jq .inbounds[].settings.clients[$opcion].alterId $config)
		add=$(jq '.inbounds[].domain' $config) && [[ $add = null ]] && add=$(wget -qO- ipv4.icanhazip.com)
		host=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $host = null ]] && host='aqui.tu.host'
		net=$(jq '.inbounds[].streamSettings.network' $config)
		[[ $net = '"grpc"' ]] && path=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || path=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
		port=$(jq '.inbounds[].port' $config)
		tls=$(jq '.inbounds[].streamSettings.security' $config)
		addip=$(wget -qO- ifconfig.me)
		clear
		msg -bar3
		blanco " Usuario: $ps"
		msg -bar3
		col2 "Remarks:" "$ps"
		col2 "Domain:" "$add" 
		col2 "IP-Address:" "$addip"
		col2 "Port:" "$port"
		col2 "id:" "$id"
		col2 "alterId:" "$aid"
		col2 "network:" "$net"
		[[ $tls = '"tls"' ]] && col2 "TLS:" "ABIERTO" || col2 "TLS:" " CERRADO"
		[[ $net = '"grpc"' ]] && col2 "Mode:" " GUN" || col2 "Head Type:" "none"
		col2 "security:" "none"
		[[ ! $host = '' ]] && col2 "Host/SNI:" "$host"
		[[ $net = '"grpc"' ]] && col2 "ServiceName:" "$path" || col2 "Path:" "$path"
		msg -bar3
		blanco "              VMESS LINK CONFIG"
		msg -bar3
		vmess
		msg -bar3
		continuar
		read foo
	done
}

vmess() {
[[ $net = '"grpc"' ]] && echo -e "\033[3;32mvmess://$(echo {\"v\": \"2\", \"ps\": $ps, \"add\": $addip, \"port\": $port, \"aid\": $aid, \"type\": \"none\", \"net\": $net, \"path\": $path, \"host\": $host, \"id\": $id, \"tls\": $tls} | base64 -w 0)\033[3;32m" || {
[[ $net = '"ws"' ]] && echo -e "\033[3;32mvmess://$(echo {\"v\": \"2\", \"ps\": $ps, \"add\": $addip, \"port\": $port, \"aid\": $aid, \"type\": \"gun\", \"net\": $net, \"path\": $path, \"host\": $host, \"id\": $id, \"tls\": $tls} | base64 -w 0)\033[3;32m"
}
}

alterid(){
	while :
	do
		aid=$(jq '.inbounds[].settings.clients[0].alterId' $config)
	clear
	msg -bar3
	blanco "        configuracion alterId"
	msg -bar3
	col2 "	alterid:" "$aid"
	msg -bar3
	col "x)" "VOLVER"
	msg -bar3
	blanco "NUEVO VALOR" 0
	read opcion

	[[ -z $opcion ]] && vacio && sleep 0.3 && break
	[[ $opcion = x ]] && break

	mv $config $temp
	new=".inbounds[].settings.clients[0]"
	echo jq \'$new += \{alterId:${opcion}\}\' $temp \> $config | bash
	chmod 777 $config
	rm $temp
	clear
	msg -bar3
	blanco "Nuevo AlterId fijado"
	msg -bar3
	restart_v2r
	done
}

port(){
	while :
	do
	port=$(jq '.inbounds[].port' $config)
	clear
	msg -bar3
	blanco "       configuracion de puerto"
	msg -bar3
	col2 " Puerto:" "$port"
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco "NUEVO PUERTO" 0
	read opcion

	[[ -z $opcion ]] && vacio && sleep 0.3 && break
	[[ $opcion = 0 ]] && break

	mv $config $temp
	new=".inbounds[]"
	echo jq \'$new += \{port:${opcion}\}\' $temp \> $config | bash
	chmod 777 $config
	rm $temp
	clear
	msg -bar3
	blanco "	Nuevo Puerto fijado"
	msg -bar3
	sleep 0.2
	restart_v2r
	done
}

address(){
	while :
	do
	add=$(jq '.inbounds[].domain' $config) && [[ $add = null ]] && add=$(wget -qO- ipv4.icanhazip.com)
	clear
	msg -bar3
	blanco "       configuracion address"
	msg -bar3
	col2 "address:" "$add"
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco "NUEVO ADDRESS" 0
	read opcion

	[[ -z $opcion ]] && vacio && sleep 0.3 && break
	[[ $opcion = 0 ]] && break

	mv $config $temp
	echo "cat $temp | jq '.inbounds[] += {domain:\"$opcion\"}' >> $config" | bash
	chmod 777 $config
	rm $temp
	clear
	msg -bar3
	blanco "Nuevo address fijado"
	msg -bar3
	restart_v2r
	sleep 0.2
	done
}

host(){
	while :
	do
	host=$(jq '.inbounds[].streamSettings.wsSettings.headers.Host' $config) && [[ $host = null ]] && host='sin host'
	clear
	msg -bar3
	blanco "       configuracion Host"
	msg -bar3
	col2 "Host:" "$host"
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco "NUEVO HOST" 0
	read opcion

	[[ -z $opcion ]] && vacio && sleep 0.3 && break
	[[ $opcion = 0 ]] && break
	mv $config $temp
	echo "cat $temp | jq '.inbounds[].streamSettings.wsSettings.headers += {Host:\"$opcion\"}' >> $config" | bash
	chmod 777 $config
	rm $temp
	clear
	msg -bar3
	blanco "Nuevo Host fijado"
	msg -bar3
	restart_v2r
	sleep 0.2
	done
}

path(){
	while :
	do
	net=$(jq '.inbounds[].streamSettings.network' $config)
	[[ $net = '"grpc"' ]] && path=$(jq '.inbounds[].streamSettings.grpcSettings.serviceName'  $config) || path=$(jq '.inbounds[].streamSettings.wsSettings.path' $config)
	clear
	msg -bar3
	blanco "       configuracion Path"
	msg -bar3
	col2 "path:" "$path"
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco "NUEVO Path" 0
	read opcion
	[[ -z $opcion ]] && vacio && sleep 0.3 && break
	[[ $opcion = 0 ]] && break
	mv $config $temp
	[[ $net = '"grpc"' ]] && echo "cat $temp | jq '.inbounds[].streamSettings.grpcSettings += {serviceName :\"$opcion\"}' >> $config" | bash || echo "cat $temp | jq '.inbounds[].streamSettings.wsSettings += {path:\"$opcion\"}' >> $config" | bash
#	echo "cat $temp | jq '.inbounds[].streamSettings.grpcSettings += {serviceName :\"$opcion\"}' >> $config" | bash
	chmod 777 $config
	rm $temp
	clear
	msg -bar3
	blanco "Nuevo path fijado"
	msg -bar3
	sleep 0.2
	restart_v2r
	done
}

crt_man(){
	while :
	do
		clear
		msg -bar3
		blanco "configuracion de certificado manual"
		msg -bar3

		chek=$(jq '.inbounds[].streamSettings.tlsSettings' $config)
		[[ ! $chek = {} ]] && {
			crt=$(jq '.inbounds[].streamSettings.tlsSettings.certificates[].certificateFile' $config)
			key=$(jq '.inbounds[].streamSettings.tlsSettings.certificates[].keyFile' $config)
			dom=$(jq '.inbounds[].domain' $config)
			echo -e "		\033[4;49minstalado\033[0m"
			col2 "crt:" "$crt"
			col2 "key:" "$key"
			col2 "dominio:" "$dom"
		} || {
			blanco "	certificado no instalado"
		}

		msg -bar3
		col "1)" "ingresar nuevo crt"
		msg -bar3
		col "0)" "VOLVER"
		msg -bar3
		blanco "opcion" 0
		read opcion

		[[ -z $opcion ]] && vacio && sleep 0.3 && break
		[[ $opcion = 0 ]] && break

		clear
		msg -bar3
		blanco "ingrese su archivo de certificado\n ej: /root/crt/certif.crt"
		msg -bar3
		blanco "crt" 0
		read crts

		clear
		msg -bar3
		blanco "	nuevo certificado"
		msg -bar3
		blanco "	$crts"
		msg -bar3
		blanco "ingrese su archivo key\n ej: /root/crt/certif.key"
		msg -bar3
		blanco "key" 0
		read keys

		clear
		msg -bar3
		blanco "	nuevo certificado"
		msg -bar3
		blanco "	$crts"
		blanco "	$keys"
		msg -bar3
		blanco "ingrese su dominio\n ej: netfree.xyz"
		msg -bar3
		blanco "dominio" 0
		read domi

		clear
		msg -bar3
		blanco "verifique sus datos sean correctos!"
		msg -bar3
		blanco "	$crts"
		blanco "	$keys"
		blanco "	$domi"
		msg -bar3
		continuar
		read foo

		mv $config $temp
		echo "cat $temp | jq '.inbounds[].streamSettings.tlsSettings += {certificates:[{certificateFile:\"$crts\",keyFile:\"$keys\"}]}' | jq '.inbounds[] += {domain:\"$domi\"}' | jq '.inbounds[].streamSettings += {security:\"tls\"}' >> $config" | bash
		chmod 777 $config
		rm $temp
		clear
		msg -bar3
		blanco "nuevo certificado agregado"
		msg -bar3
		restart_v2r
		sleep 0.2
	done
}

install(){
	clear
	install_ini
	msg -bar3
	blanco "	Esta por intalar v2ray!"
	echo "source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/menu_inst/v2ray_manager.url.sh)" > /bin/v2r.sh
	chmod +x /bin/v2r.sh
	msg -bar3
	blanco " La instalacion puede tener\n alguna fallas!\n por favor observe atentamente\n el log de intalacion,\n este podria contener informacion\n sobre algunos errores!\n estos deveras ser corregidos de\n forma manual antes de continual\n usando el script"
	msg -bar3
	sleep 0.2
	blanco "Enter para continuar..."
	read foo
	config='/etc/v2ray/config.json'
    tmp='/etc/v2ray/temp.json'
	source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/menu_inst/v2ray.sh)
echo '[Unit]
Description=V2Ray Service
After=network.target nss-lookup.target
StartLimitIntervalSec=0

[Service]
Type=simple
User=root
CapabilityBoundingSet=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
AmbientCapabilities=CAP_NET_ADMIN CAP_NET_BIND_SERVICE
NoNewPrivileges=true
ExecStart=/usr/bin/v2ray/v2ray -config /etc/v2ray/config.json
Restart=always
RestartSec=3s


[Install]
WantedBy=multi-user.target' > /etc/systemd/system/v2ray.service
systemctl daemon-reload &>/dev/null
systemctl start v2ray &>/dev/null
systemctl enable v2ray &>/dev/null
systemctl restart v2ray.service
clear&&clear
	title "   INSTALACION DE XRAY MOD MENU "
	echo -e " \033[0;31mEsta opcion es aparte, para habilitar XRAY Install"
	echo -e " Habilitaremos el modulo XRAY previo al V2RAY ya instalado \033[0m"
		echo -e "  Accederas al pannel original si es la primera vez !!\n\033[0m"
	msg -bar3
	blanco " Deseas instalar el XRAY ? [S/N]" 0
	read opcion
	msg -bar3
	case $opcion in
		[Ss]|[Yy]) 
					msg -bar3
					[[ -e /bin/xr.sh ]] && xr.sh || {
					xray
					echo "source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/xray_manager.sh)" > /bin/xr.sh
					chmod +x /bin/xr.sh
							clear
							msg -bar3
							blanco " Se ha agregado un autoejecutor en el Sector de Inicios Rapidos"
							msg -bar3
							blanco "	  Para Acceder al menu Rapido \n	     Utilize * xr.sh * !!!"
							msg -bar3
							echo -e "		\033[4;31mNOTA importante\033[0m"
							echo -e " \033[0;31mSi deseas desabilitar esta opcion, apagala"
							echo -e " Y te recomiendo, no alterar nada en este menu, para"
							echo -e "             Evitar Errores Futuros"
							echo -e "     ESTE ES UN DUPLICADO V2RAY CON MODULO XRAY"
							echo -e " y causar problemas en futuras instalaciones.\033[0m"
							msg -bar3
							continuar
							read foo
					}
		;;
		[Nn]) continuar && read foo;;
	esac
#restart_v2r
}

v2ray_tls(){
	clear
	msg -bar3
	blanco "		certificado tls v2ray"
	echo -e "Ingrese Correo Temporal o Fijo \n  Para Validar su Cerficicado SSL " 
	read -p " Ejemplo > email=my@example.com : " -e -i $(date | md5sum | head -c15)@gmail.com crreo
	msg -bar3
	wget -O -  https://get.acme.sh | sh -s email=$crreo
	v2ray tls
	msg -bar3
	continuar
	read foo
}

v2ray_stream(){
	clear
	msg -bar3
	blanco "	instalacion de protocolos v2ray"
	msg -bar3
	v2ray stream
	msg -bar3
	continuar
	read foo
}

v2ray_menu(){
	clear
	msg -bar3
	blanco "		MENU V2RAY"
	msg -bar3
	v2ray
}

backups(){
	while :
	do
		unset opcion
		unset PID
		if [[ $(ps x | grep "http-server" | grep -v grep) = "" ]]; then
			PID="\033[0;31m[offline]"
		else
			PID="\033[1;92m[online]"
		fi

	clear
	title "	Config de Respaldos"
	col "1)" "Respaldar Ahora"
	col "2)" "\033[1;92mRestaurar Respaldo"
	col "3)" "\033[0;31mEliminiar Respaldos"
	col "4)" "\033[1;34mRespaldo en linea $PID"
	col "5)" "\033[1;33mRespaldos automatico $(on_off_res)"
	msg -bar3
	
	col "6)" "\033[1;33m RESTAURAR Online PORT :81 "
	msg -bar3
	col "0)" "VOLVER"
	msg -bar3
	blanco "opcion" 0
	read opcion

	case $opcion in
		1)	autBackup
			clear
			title "	Nuevo Respaldo Creado..."
			sleep 0.2;;
		2)	restore;;
		3)	rm -rf $backdir/*.tar
			clear
			title "	Almacer de Respaldo limpia..."
			sleep 0.2;;
		4)	server;;


		5)	if [[ $(cat $v2rdir/conf | grep "autBackup" | cut -d " " -f2) = "0" ]]; then
				sed -i 's/autBackup 0/autBackup 1/' $v2rdir/conf
			else
				sed -i 's/autBackup 1/autBackup 0/' $v2rdir/conf
			fi;;
		6)
		clear
		echo -e "\033[0;33m
         ESTA FUNCION EXPERIMENTAL 
Una vez que se descarge tu Fichero, Escoje el BackOnline
	
				  + OJO +
				 
   Luego de Restaurarlo, Vuelve Activar el TLS 
 Para Validar la Configuracion de tu certificao"
msg -bar3
echo -n "INGRESE LINK Que Mantienes Online en GitHub, o VPS \n" 
read -p "Pega tu Link : " url1
wget -q -O $backdir/BakcOnline.tar $url1 && echo -e "\033[1;31m- \033[1;32mFile Exito!"  && restore || echo -e "\033[1;31m- \033[1;31mFile Fallo" && sleep 0.2
		;;
		0)	break;;
		*)	blanco "opcion incorrecta..." && sleep 0.2;;
	esac
	done
}


restablecer_v2r(){
	clear
	title "   restablecer ajustes v2ray"
	echo -e " \033[0;31mEsto va a restablecer los\n ajustes predeterminados de v2ray"
	echo -e " Se perdera ajuste previos,\n incluido los Usuarios\033[0m"
		echo -e "  LUEGO DE ESTO, DEBERAS RECONFIGURAR TU METODO\n\033[0m"
	msg -bar3
	blanco "quiere continuar? [S/N]" 0
	read opcion
	msg -bar3
	case $opcion in
		[Ss]|[Yy]) 
		v2ray new && rm -f ${configLOCK}
		rm -f /etc/v2r/user
		;;
		[Nn]) continuar && read foo;;
	esac
}

remove_all(){
	sed -i '/net.core.default_qdisc/d' /etc/sysctl.conf
    sed -i '/net.ipv4.tcp_congestion_control/d' /etc/sysctl.conf
    sed -i '/fs.file-max/d' /etc/sysctl.conf
	sed -i '/net.core.rmem_max/d' /etc/sysctl.conf
	sed -i '/net.core.wmem_max/d' /etc/sysctl.conf
	sed -i '/net.core.rmem_default/d' /etc/sysctl.conf
	sed -i '/net.core.wmem_default/d' /etc/sysctl.conf
	sed -i '/net.core.netdev_max_backlog/d' /etc/sysctl.conf
	sed -i '/net.core.somaxconn/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_syncookies/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_tw_reuse/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_tw_recycle/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_fin_timeout/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_keepalive_time/d' /etc/sysctl.conf
	sed -i '/net.ipv4.ip_local_port_range/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_syn_backlog/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_tw_buckets/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_rmem/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_wmem/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_mtu_probing/d' /etc/sysctl.conf
	sed -i '/net.ipv4.ip_forward/d' /etc/sysctl.conf
	sed -i '/fs.inotify.max_user_instances/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_syncookies/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_fin_timeout/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_tw_reuse/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_syn_backlog/d' /etc/sysctl.conf
	sed -i '/net.ipv4.ip_local_port_range/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_tw_buckets/d' /etc/sysctl.conf
	sed -i '/net.ipv4.route.gc_timeout/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_synack_retries/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_syn_retries/d' /etc/sysctl.conf
	sed -i '/net.core.somaxconn/d' /etc/sysctl.conf
	sed -i '/net.core.netdev_max_backlog/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_timestamps/d' /etc/sysctl.conf
	sed -i '/net.ipv4.tcp_max_orphans/d' /etc/sysctl.conf
	clear
	echo -e "  \033[0;92mLa aceleración está Desinstalada."
	sleep 0.1
}

bbr(){
	while :
	do
	clear
	title "		ACELERACION BBR"
	blanco "	Esto activara la aceleracion\n	por defecto de su kernel.\n	no se modoficar nada del sistema."
	msg -bar3
	col "1)" "Acivar aceleracion"
	col "2)" "quitar toda aceleracion"
	msg -bar3
	col "0)" "volver"
	msg -bar3
	blanco "opcion" 0
	read opcion
	case $opcion in
		1)
			remove_all
			echo "net.core.default_qdisc=fq" >> /etc/sysctl.conf
			echo "net.ipv4.tcp_congestion_control=bbr" >> /etc/sysctl.conf
			sysctl -p
			echo -e "  \033[0;92m¡BBR comenzó con éxito!";;
		2)remove_all;;
		0)break;;
		*)blanco " seleccione una opcion" && sleep 0.2;;
	esac
	done
}

settings(){
	while :
	do
	clear
	msg -bar3
	blanco "	  Ajustes e instalacion v2ray"
	msg -bar3
	col "1)" "CAMBIAR DOMINIO / IP"
	col "2)" "CAMBIAR PUERTO "
	col "3)" "CAMBIAR AlterId"
	col "4)" "CAMBIAR HOST"
	col "5)" "CAMBIAR PACH"
	msg -bar3
	col "6)" "AÑADIR CRT (script)"
	col "7)" "AÑADIR CRT V2RAY NATIVO"
	col "8)" "AÑADIR CERTIFICADO MANUAL"
	msg -bar3
	col "9)" "CAMBIAR PROTOCOLO V2RAY"
	col "10)" "ENTRAR A V2RAY ORIGINAL"
	col "11)" "RESTABLECER CONFIGURACION"
	msg -bar3
	col "12)" "BBR nativo del sistema"
	col "13)" "INSTALL/REINSTALL V2RAY/XRAY"
	msg -bar3
	col "14)" "Conf. COPIAS DE SEGURIDAD"
	msg -bar3
	#col "15)" "HABILITAR MODULO XRAY ( V2ray )"
	#msg -bar3
	col "0)" "REGRESAR"
	msg -bar3
	blanco "opcion" 0
	read opcion

	[[ -z $opcion ]] && vacio && sleep 0.3 && break
	[[ $opcion = 0 ]] && break

	case $opcion in
		1)address;;
		2)port;;
		3)alterid;;
		4)host;;
		5)path;;
		6)domain_check && clear ;;
		7)v2ray_tls;;
		8)crt_man;;
		9)v2ray_stream;;
		10)v2ray_menu;;
		11)restablecer_v2r;;
		12)bbr;;
		13)install;;
		14)backups;;
		15)_xray;;
		*) blanco " solo numeros de 0 a 14" && sleep 0.2;;
	esac
    done
}

_xray() {
[[ -e /bin/xr.sh ]] && xr.sh || {
xray
echo "source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/xray_manager.sh)" > /bin/xr.sh
chmod +x /bin/xr.sh
		clear
		msg -bar3
		blanco " Se ha agregado un autoejecutor en el Sector de Inicios Rapidos"
		msg -bar3
		blanco "	  Para Acceder al menu Rapido \n	     Utilize * xr.sh * !!!"
		msg -bar3
		echo -e "		\033[4;31mNOTA importante\033[0m"
		echo -e " \033[0;31mSi deseas desabilitar esta opcion, apagala"
		echo -e " Y te recomiendo, no alterar nada en este menu, para"
		echo -e "             Evitar Errores Futuros"
		echo -e "     ESTE ES UN DUPLICADO V2RAY CON MODULO XRAY"
		echo -e " y causar problemas en futuras instalaciones.\033[0m"
		msg -bar3
		continuar
		read foo
}
}

enon(){
echo "source <(curl -sSL https://raw.githubusercontent.com/emirjorge/Script-Z/master/CHUMO/Recursos/menu_inst/v2ray_manager.url.sh)" > /bin/v2r.sh
chmod +x /bin/v2r.sh
		clear
		msg -bar3
		blanco " Se ha agregado un autoejecutor en el Sector de Inicios Rapidos"
		msg -bar3
		blanco "	  Para Acceder al menu Rapido \n	     Utilize * v2r.sh * !!!"
		msg -bar3
		echo -e "		\033[4;31mNOTA importante\033[0m"
		echo -e " \033[0;31mSi deseas desabilitar esta opcion, apagala"
		echo -e " Y te recomiendo, no alterar nada en este menu, para"
		echo -e "             Evitar Errores Futuros"
		echo -e " y causar problemas en futuras instalaciones.\033[0m"
		msg -bar3
		continuar
		read foo
}
enoff(){
rm -f /bin/v2r.sh
		msg -bar3
		echo -e "		\033[4;31mNOTA importante\033[0m"
		echo -e " \033[0;31mSe ha Desabilitado el menu Rapido de v2r.sh"
		echo -e " Y te recomiendo, no alterar nada en este menu, para"
		echo -e "             Evitar Errores Futuros"
		echo -e " y causar problemas en futuras instalaciones.\033[0m"
		msg -bar3
		continuar
		read foo
}

enttrada () {

	while :
	do
	clear
	msg -bar3
	blanco "	  Ajustes de Entrada Rapida de Menu v2ray"
	msg -bar3
	col "1)" "Habilitar v2r.sh, Como entrada Rapida"
	col "2)" "Eliminar v2r.sh, Como entrada Rapida"
	msg -bar3
	col "0)" "Volver"
	msg -bar3
	blanco "opcion" 0
	read opcion

	[[ -z $opcion ]] && vacio && sleep 0.3 && break
	[[ $opcion = 0 ]] && break

	case $opcion in
		1)enon;;
		2)enoff;;
		*) blanco " solo numeros de 0 a 2" && sleep 0.1;;
	esac
    done

}

_fix () {
restablecer_v2r
}

main(){
	[[ ! -e $config ]] && {
		clear
		msg -bar3
		blanco " No se encontro ningun archovo de configracion v2ray"
		msg -bar3
		blanco "	  No instalo v2ray o esta usando\n	     una vercion diferente!!!"
		msg -bar3
		echo -e "		\033[4;31mNOTA importante\033[0m"
		echo -e " \033[0;31mSi esta usando una vercion v2ray diferente"
		echo -e " y opta por cuntinuar usando este script."
		echo -e " Este puede; no funcionar correctamente"
		echo -e " y causar problemas en futuras instalaciones.\033[0m"
		msg -bar3
		continuar
		read foo
	}
	while :
	do
		_usor=$(printf '%-8s' "$(free -m | awk 'NR==2{printf "%.2f%%", $3*100/$2 }')")
		_usop=$(printf '%-1s' "$(top -bn1 | awk '/Cpu/ { cpu = "" 100 - $8 "%" }; END { print cpu }')")
		[[ -e /bin/v2r.sh ]] && enrap="\033[1;92m[ON]" || enrap="\033[0;31m[OFF]"
		[[ -e /etc/v2ray/config.json ]] && _v2Reg="$(cat /etc/v2ray/config.json | jq .inbounds[].settings.clients[].email|wc -l)" || _v2Reg=0
		[[ -e /etc/v2ray/config.json.lock ]] && _v2LOCK="$(cat /etc/v2ray/config.json.lock|wc -l)" || _v2LOCK=0
		clear
		title2
		title "   Ram: \033[1;32m$_usor  \033[0;31m<<< \033[1;37mMENU V2RAY \033[0;31m>>>  \033[1;37mCPU: \033[1;32m$_usop"
		col "1)" "CREAR NUEVO USUARIO"
		col "2)" "\033[0;92mRENOVAR USUARIO"
		col "3)" "\033[0;31mREMOVER USUARIO"
		col "4)" "VER DATOS DE USUARIOS \033[1;32m ( ${_v2Reg} )"
		col "5)" "VER USUARIOS CONECTADOS"
		col "b)" "LOCK/UNLOCK USUARIO \033[1;32m ( ${_v2LOCK} )"
		msg -bar3
		col "6)" "\033[1;33m AJUSTES V2RAY $_v2"
		msg -bar3
		col "7)" "\033[1;33mENTRAR CON \033[1;33mv2r.sh $enrap"
		msg -bar3
		col "8)" "\033[1;33mFIXEAR V2RAY ( SOLO USUARIOS )"
		msg -bar3
		col "0)" "SALIR \033[0;31m|| $(blanco "Respaldos a utomaticos") $(on_off_res)"
		msg -bar3
		blanco "opcion" 0
		read opcion

		case $opcion in
			1) add_user;;
			2) renew;;
			3) dell_user;;
			4) view_user;;
			5) usrCONEC ;;
			6) settings;;
			7) enttrada;;
			8) restablecer_v2r;;
			b) _lo_un;;
			0) break;;
			*) blanco "\n selecione una opcion del 0 al 8" && sleep 0.1;;
		esac
	done
}

[[ $1 = "autoDel" ]] && {
	autoDel
} || {
	autoDel
	main
}
}


case $1 in
 -r| --panel-rufu| --rufu) v2ray_r ;;
 --default) v2ray_f;;
 --menu-dos)v2ray_s;;
esac
